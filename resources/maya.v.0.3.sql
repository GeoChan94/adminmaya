-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-11-2018 a las 11:58:34
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `maya`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracteristica`
--

CREATE TABLE `caracteristica` (
  `id_caracteristica` int(11) NOT NULL,
  `pisos` tinyint(4) DEFAULT NULL,
  `orientacion` varchar(10) DEFAULT NULL,
  `estacionamientos` tinyint(4) DEFAULT NULL,
  `antiguedad` tinyint(4) DEFAULT NULL,
  `banios` tinyint(4) DEFAULT NULL,
  `dormitorios` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria`
--

CREATE TABLE `galeria` (
  `id_galeria` int(11) NOT NULL,
  `uri_galeria` varchar(300) NOT NULL,
  `tipo` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modalidad_previo`
--

CREATE TABLE `modalidad_previo` (
  `id_modalidad_previo` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `modalidad_previo`
--

INSERT INTO `modalidad_previo` (`id_modalidad_previo`, `nombre`, `descripcion`) VALUES
(1, 'venta', ''),
(2, 'alquiler', ''),
(3, 'proyecto', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `previo`
--

CREATE TABLE `previo` (
  `id_previo` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `descripcion_previo` text NOT NULL,
  `ubicacion_geografica` varchar(50) NOT NULL,
  `mantenimiento` decimal(10,0) DEFAULT '0',
  `area` double NOT NULL,
  `medida_frente` double NOT NULL,
  `medida_fondo` double NOT NULL,
  `uri_imagen_destacada` varchar(100) NOT NULL,
  `precio` double NOT NULL,
  `creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uri_previo` varchar(150) NOT NULL,
  `tipo_pago_id_tipo_pago` int(11) NOT NULL,
  `tipo_previo_id_tipo_previo` int(11) NOT NULL,
  `provincia_previo_id_provincia` int(11) NOT NULL,
  `modalidad_previo_id_modalidad_previo` int(11) NOT NULL,
  `tipo_documento_previo_id_tipo_documento_previo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `previo`
--

INSERT INTO `previo` (`id_previo`, `titulo`, `descripcion_previo`, `ubicacion_geografica`, `mantenimiento`, `area`, `medida_frente`, `medida_fondo`, `uri_imagen_destacada`, `precio`, `creacion`, `uri_previo`, `tipo_pago_id_tipo_pago`, `tipo_previo_id_tipo_previo`, `provincia_previo_id_provincia`, `modalidad_previo_id_modalidad_previo`, `tipo_documento_previo_id_tipo_documento_previo`) VALUES
(1, 'titulo', 'asdasdasd', 'ubi', '0', 100, 10, 20, 'uriimg', 150.34, '2018-11-06 23:03:27', 'uqwerq-qwe-df', 4, 5, 1, 3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `previo_has_caracteristica`
--

CREATE TABLE `previo_has_caracteristica` (
  `id_previo_has_caracteristicacol` int(11) NOT NULL,
  `previo_id_previo` int(11) NOT NULL,
  `caracteristica_id_caracteristica` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `previo_has_galeria`
--

CREATE TABLE `previo_has_galeria` (
  `id_previo_has_galeriacol` int(11) NOT NULL,
  `previo_id_previo` int(11) NOT NULL,
  `galeria_id_galeria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `previo_has_servicio`
--

CREATE TABLE `previo_has_servicio` (
  `id_previo_has_serviciocol` int(11) NOT NULL,
  `previo_id_previo` int(11) NOT NULL,
  `servicio_id_servicio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia_previo`
--

CREATE TABLE `provincia_previo` (
  `id_provincia` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `provincia_previo`
--

INSERT INTO `provincia_previo` (`id_provincia`, `nombre`) VALUES
(1, 'puno'),
(2, 'juliaca');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `id_servicio` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`id_servicio`, `nombre`, `descripcion`) VALUES
(1, 'luz', ''),
(2, 'agua', ''),
(3, 'desague', ''),
(4, 'internet', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento_previo`
--

CREATE TABLE `tipo_documento_previo` (
  `id_tipo_documento_previo` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_documento_previo`
--

INSERT INTO `tipo_documento_previo` (`id_tipo_documento_previo`, `nombre`, `descripcion`) VALUES
(1, 'registro publico', ''),
(2, 'escritura', ''),
(3, 'posesión ', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_pago`
--

CREATE TABLE `tipo_pago` (
  `id_tipo_pago` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_pago`
--

INSERT INTO `tipo_pago` (`id_tipo_pago`, `nombre`, `descripcion`) VALUES
(1, 'anticretico', ''),
(2, 'mensual', ''),
(3, 'anual', ''),
(4, 'cuotas', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_previo`
--

CREATE TABLE `tipo_previo` (
  `id_tipo_previo` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_previo`
--

INSERT INTO `tipo_previo` (`id_tipo_previo`, `nombre`, `descripcion`) VALUES
(1, 'departamento', ''),
(2, 'casa', ''),
(3, 'terreno urbano', ''),
(4, 'terreno rural', ''),
(5, 'terreno turistico', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id_tipo_usuario` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id_tipo_usuario`, `nombre`, `descripcion`) VALUES
(1, 'admin', 'administrador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `contrasenia` varchar(20) NOT NULL,
  `tipo_usuario_id_tipo_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `caracteristica`
--
ALTER TABLE `caracteristica`
  ADD PRIMARY KEY (`id_caracteristica`);

--
-- Indices de la tabla `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id_galeria`);

--
-- Indices de la tabla `modalidad_previo`
--
ALTER TABLE `modalidad_previo`
  ADD PRIMARY KEY (`id_modalidad_previo`);

--
-- Indices de la tabla `previo`
--
ALTER TABLE `previo`
  ADD PRIMARY KEY (`id_previo`),
  ADD KEY `fk_previo_tipo_pago1_idx` (`tipo_pago_id_tipo_pago`),
  ADD KEY `fk_previo_tipo_previo1_idx` (`tipo_previo_id_tipo_previo`),
  ADD KEY `fk_previo_provincia_previo1_idx` (`provincia_previo_id_provincia`),
  ADD KEY `fk_previo_modalidad_previo1_idx` (`modalidad_previo_id_modalidad_previo`),
  ADD KEY `fk_previo_tipo_documento_previo1_idx` (`tipo_documento_previo_id_tipo_documento_previo`);

--
-- Indices de la tabla `previo_has_caracteristica`
--
ALTER TABLE `previo_has_caracteristica`
  ADD PRIMARY KEY (`id_previo_has_caracteristicacol`),
  ADD UNIQUE KEY `previo_id_previo` (`previo_id_previo`),
  ADD UNIQUE KEY `caracteristica_id_caracteristica` (`caracteristica_id_caracteristica`),
  ADD KEY `fk_previo_has_caracteristica_caracteristica1_idx` (`caracteristica_id_caracteristica`),
  ADD KEY `fk_previo_has_caracteristica_previo1_idx` (`previo_id_previo`);

--
-- Indices de la tabla `previo_has_galeria`
--
ALTER TABLE `previo_has_galeria`
  ADD PRIMARY KEY (`id_previo_has_galeriacol`),
  ADD KEY `fk_previo_has_galeria_galeria1_idx` (`galeria_id_galeria`),
  ADD KEY `fk_previo_has_galeria_previo1_idx` (`previo_id_previo`);

--
-- Indices de la tabla `previo_has_servicio`
--
ALTER TABLE `previo_has_servicio`
  ADD PRIMARY KEY (`id_previo_has_serviciocol`),
  ADD UNIQUE KEY `previo_id_previo_UNIQUE` (`previo_id_previo`),
  ADD UNIQUE KEY `servicio_id_servicio_UNIQUE` (`servicio_id_servicio`),
  ADD KEY `fk_previo_has_servicio_servicio1_idx` (`servicio_id_servicio`),
  ADD KEY `fk_previo_has_servicio_previo_idx` (`previo_id_previo`);

--
-- Indices de la tabla `provincia_previo`
--
ALTER TABLE `provincia_previo`
  ADD PRIMARY KEY (`id_provincia`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id_servicio`);

--
-- Indices de la tabla `tipo_documento_previo`
--
ALTER TABLE `tipo_documento_previo`
  ADD PRIMARY KEY (`id_tipo_documento_previo`);

--
-- Indices de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  ADD PRIMARY KEY (`id_tipo_pago`);

--
-- Indices de la tabla `tipo_previo`
--
ALTER TABLE `tipo_previo`
  ADD PRIMARY KEY (`id_tipo_previo`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`id_tipo_usuario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `fk_usuario_tipo_usuario1_idx` (`tipo_usuario_id_tipo_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `caracteristica`
--
ALTER TABLE `caracteristica`
  MODIFY `id_caracteristica` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id_galeria` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `modalidad_previo`
--
ALTER TABLE `modalidad_previo`
  MODIFY `id_modalidad_previo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `previo`
--
ALTER TABLE `previo`
  MODIFY `id_previo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `previo_has_caracteristica`
--
ALTER TABLE `previo_has_caracteristica`
  MODIFY `id_previo_has_caracteristicacol` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `previo_has_galeria`
--
ALTER TABLE `previo_has_galeria`
  MODIFY `id_previo_has_galeriacol` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `previo_has_servicio`
--
ALTER TABLE `previo_has_servicio`
  MODIFY `id_previo_has_serviciocol` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `provincia_previo`
--
ALTER TABLE `provincia_previo`
  MODIFY `id_provincia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `id_servicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipo_documento_previo`
--
ALTER TABLE `tipo_documento_previo`
  MODIFY `id_tipo_documento_previo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  MODIFY `id_tipo_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipo_previo`
--
ALTER TABLE `tipo_previo`
  MODIFY `id_tipo_previo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `id_tipo_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `previo`
--
ALTER TABLE `previo`
  ADD CONSTRAINT `fk_previo_modalidad_previo1` FOREIGN KEY (`modalidad_previo_id_modalidad_previo`) REFERENCES `modalidad_previo` (`id_modalidad_previo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_previo_provincia_previo1` FOREIGN KEY (`provincia_previo_id_provincia`) REFERENCES `provincia_previo` (`id_provincia`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_previo_tipo_documento_previo1` FOREIGN KEY (`tipo_documento_previo_id_tipo_documento_previo`) REFERENCES `tipo_documento_previo` (`id_tipo_documento_previo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_previo_tipo_pago1` FOREIGN KEY (`tipo_pago_id_tipo_pago`) REFERENCES `tipo_pago` (`id_tipo_pago`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_previo_tipo_previo1` FOREIGN KEY (`tipo_previo_id_tipo_previo`) REFERENCES `tipo_previo` (`id_tipo_previo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `previo_has_caracteristica`
--
ALTER TABLE `previo_has_caracteristica`
  ADD CONSTRAINT `fk_previo_has_caracteristica_caracteristica1` FOREIGN KEY (`caracteristica_id_caracteristica`) REFERENCES `caracteristica` (`id_caracteristica`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_previo_has_caracteristica_previo1` FOREIGN KEY (`previo_id_previo`) REFERENCES `previo` (`id_previo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `previo_has_galeria`
--
ALTER TABLE `previo_has_galeria`
  ADD CONSTRAINT `fk_previo_has_galeria_galeria1` FOREIGN KEY (`galeria_id_galeria`) REFERENCES `galeria` (`id_galeria`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_previo_has_galeria_previo1` FOREIGN KEY (`previo_id_previo`) REFERENCES `previo` (`id_previo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `previo_has_servicio`
--
ALTER TABLE `previo_has_servicio`
  ADD CONSTRAINT `fk_previo_has_servicio_previo` FOREIGN KEY (`previo_id_previo`) REFERENCES `previo` (`id_previo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_previo_has_servicio_servicio1` FOREIGN KEY (`servicio_id_servicio`) REFERENCES `servicio` (`id_servicio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_tipo_usuario1` FOREIGN KEY (`tipo_usuario_id_tipo_usuario`) REFERENCES `tipo_usuario` (`id_tipo_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
