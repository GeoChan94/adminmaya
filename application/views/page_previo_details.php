<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title></title>
    <?php
        $this->load->view('recursos/css');
    ?>  
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.css' rel='stylesheet' />
    <style type="text/css">
        .div-carousel{
            height: 400px;
        }
    </style>
</head>
<body class="bg-light m-0 p-0">
    <?php
        // $this->load->view('menu/header');
        $this->load->view('menu/web/menu');
    ?> 
    <content data-spy="scroll" data-target="#main-menu" data-offset="0" class="bg-light pt-4 pb-4">
	    <?php
        // var_dump($proyecto);
	        $this->load->view('page/page_previo_details');
	    ?>
	</content>  
<?php
    $this->load->view('recursos/js');
?> 
<?php
    // $this->load->view('footer/footer');
?>    
<script type="text/javascript">
    // Tooltips Initialization
 $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

    $( ".btn_mensaje" ).on( "click", function() {
    var id_previo=$(this).data('idprevio');

    var mensaje=$('#mensaje').val();
    var nombre=$('#nombre').val();
    var correo=$('#correo').val();
    var celular=$('#celular').val();
    if (mensaje) {
        $('#mensaje').removeClass('is-invalid').addClass('is-valid');
    }else{
        $('#mensaje').removeClass('is-valid').addClass('is-invalid');
    }
    if (nombre) {
        $('#nombre').removeClass('is-invalid').addClass('is-valid');
    }else{
        $('#nombre').removeClass('is-valid').addClass('is-invalid');
    }
    if (correo) {
        $('#correo').removeClass('is-invalid').addClass('is-valid');
    }else{
        $('#correo').removeClass('is-valid').addClass('is-invalid');
    }
    if (celular) {
        $('#celular').removeClass('is-invalid').addClass('is-valid');
    }else{
        $('#celular').removeClass('is-valid').addClass('is-invalid');
    }
    if (mensaje&&nombre&&correo&&celular&&id_previo) {
        $.ajax({
            data: {"mensaje" : mensaje , "nombre" : nombre, "correo" : correo, "celular" : celular, "id_previo" : id_previo},
            //Cambiar a type: POST si necesario
            type: "POST",
            // Formato de datos que se espera en la respuesta
            // dataType: "json",


            // URL a la que se enviará la solicitud Ajax
            url: '<?php echo base_url()?>mensaje/add',
        })
         .done(function( data, textStatus, jqXHR ) {
             if ( console && console.log ) {
                $('#mensaje').val('');
                $('#nombre').val('');
                $('#correo').val('');
                $('#celular').val('');
                $('.msn_mensaje').html('<span class="fa fa-check"></span> Mensaje enviado');
                 // console.log( "La solicitud se ha completado correctamente.",data,textStatus);
                 // location.reload();
             }
         })
         .fail(function( jqXHR, textStatus, errorThrown ) {
             if ( console && console.log ) {
                 console.log( "La solicitud a fallado: " +  textStatus,jqXHR);
             }
        });

    }else{
        console.log('error');
    }

    
    });
</script>
</body>

</html>