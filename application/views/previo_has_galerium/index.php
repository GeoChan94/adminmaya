<div class="pull-right">
	<a href="<?php echo site_url('admin/previo_has_galerium/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Previo Has Galeriacol</th>
		<th>Previo Id Previo</th>
		<th>Galeria Id Galeria</th>
		<th>Actions</th>
    </tr>
	<?php foreach($previo_has_galeria as $p){ ?>
    <tr>
		<td><?php echo $p['id_previo_has_galeriacol']; ?></td>
		<td><?php echo $p['previo_id_previo']; ?></td>
		<td><?php echo $p['galeria_id_galeria']; ?></td>
		<td>
            <a href="<?php echo site_url('admin/previo_has_galerium/edit/'.$p['id_previo_has_galeriacol']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('admin/previo_has_galerium/remove/'.$p['id_previo_has_galeriacol']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
