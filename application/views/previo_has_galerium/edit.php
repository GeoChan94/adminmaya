<?php echo form_open('admin/previo_has_galerium/edit/'.$previo_has_galerium['id_previo_has_galeriacol'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="previo_id_previo" class="col-md-4 control-label"><span class="text-danger">*</span>Previo</label>
		<div class="col-md-8">
			<select name="previo_id_previo" class="form-control">
				<option value="">select previo</option>
				<?php 
				foreach($all_previo as $previo)
				{
					$selected = ($previo['id_previo'] == $previo_has_galerium['previo_id_previo']) ? ' selected="selected"' : "";

					echo '<option value="'.$previo['id_previo'].'" '.$selected.'>'.$previo['id_previo'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('previo_id_previo');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="galeria_id_galeria" class="col-md-4 control-label"><span class="text-danger">*</span>Galerium</label>
		<div class="col-md-8">
			<select name="galeria_id_galeria" class="form-control">
				<option value="">select galerium</option>
				<?php 
				foreach($all_galeria as $galerium)
				{
					$selected = ($galerium['id_galeria'] == $previo_has_galerium['galeria_id_galeria']) ? ' selected="selected"' : "";

					echo '<option value="'.$galerium['id_galeria'].'" '.$selected.'>'.$galerium['id_galeria'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('galeria_id_galeria');?></span>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>