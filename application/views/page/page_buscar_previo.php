<div class="container">
	
<div class="row">
	<div class="col-md-3">
		<div class="border rounded bg-white p-2 form-group">
			<h5><span class="fa fa-search"></span> Filtros aplicados</h5>
			<br>
			<span class="btn btn-sm btn-secondary ">
			  Profile <span class="fa fa-close"></span>
			  <!-- <span class="sr-only">unread messages</span> -->
			</span>
			<span class="btn btn-sm btn-secondary ">
			  filtro2 <span class="fa fa-close"></span>
			  <!-- <span class="sr-only">unread messages</span> -->
			</span>
		</div>

		<div class="border rounded bg-white p-2">
			<h5><span class="fa fa-filter"></span> Filtros</h5>
			<hr>
			<h6>Ciudad</h6>
<ul class="list-unstyled pl-2">
		<li><a href="">Lima (17,626)</a></li>
		<li><a href="">San Vicente de Cañete (83)</a></li>
		<li><a href="">Huaral (2)</a></li>
		<li><a href="">Barranca</a></li>
	</ul>		
		</div>

	</div>
	<div class="col-md-9">
		<div class="form-group">
			<span><b><?=count($previo);?></b> Previos en <?=$modalidad['nombre'];?> en la Provincia de <?=$provincia['nombre'];?></span>
		</div>
	
		<div class="row  form-group">
			<div class="col-auto text-uppercase font-weight-bold">venta</div>
			<div class="col-auto text-uppercase font-weight-bold">alquiler</div>
			<div class="col-auto text-uppercase font-weight-bold">proyectos</div>		
		</div>
		<div class="col-md-12 p-0  ">
			<div class="row float-right form-group">
				<div class="col-auto text-capitalize ">ordenar por</div>
				<div class="col-auto text-uppercase font-weight-bold">recien publicados</div>
				<div class="col-auto text-uppercase font-weight-bold">mas baratos</div>
				<div class="col-auto text-uppercase font-weight-bold">ver mas</div>	
			</div>
				
		</div>
		<div class="row m-0 w-100 div_previos">
			<!-- <div class="col-12 form-group card bg-white border border-warning border-left-0 border-right-0 border-bottom-0 border-height-4 p-0">
				<div class="row m-0">
					<div class="col-4 p-0">
						<img class="img-responsive w-100" src="https://img10.naventcdn.com/avisos/11/00/54/81/53/40/360x266/104230032.jpg">
						<div class="  p-2 ">
							<h5 class="">S/ 3,300</h5>
						</div>
					</div>
					<div class="col-8 p-3">
						<div class="form-group">
							<a href="<?=base_url();?>propiedades/alquiler/1" class="text-primary font-weight-bold text-uppercase ">Alquiler Departamento Triplex Lince Todo Incluido</a><br>
							<span><span class="fa fa-map-marker"></span>Jesús María, Lima</span>
						</div>
						
						<div class="form-group">
						Calle Estados Unidos, Urb. San Felipe, Dpto. 12, Jesús Maria. Precio: S/. 2800 (el pago es en soles) Precio: $ 850 (precio referencial) ac. 120 m2. Mantenimiento: S/. 217 soles, no incluye agua, solo áreas comunes, medidor de agua independiente. Departamento en alquiler en Jesús Maria, de aprox. 8 años de antigüedad, en muy buen estado. Edifici...</div>
						<div class=" row">
							<div class="col text-info">
								Publicado hace 22 días
							</div>
							<div class="col text-right">
								<span class="btn btn-outline-primary text-uppercase"> CONTACtar</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 form-group card bg-white border border-warning border-left-0 border-right-0 border-bottom-0 border-height-4 p-0">
				<div class="row m-0">
					<div class="col-4 p-0">
						<img class="img-responsive w-100" src="https://img10.naventcdn.com/avisos/11/00/53/57/39/56/1200x1200/85049594.jpg">
						<div class="  p-2 ">
							<h5 class="">S/ 3,300</h5>
						</div>
					</div>
					<div class="col-8 p-3">
						<div class="form-group">
							<a href="<?=base_url();?>propiedades/alquiler/1" class="text-primary font-weight-bold text-uppercase ">Alquiler Departamento Triplex Lince Todo Incluido</a><br>
							<span><span class="fa fa-map-marker"></span>Jesús María, Lima</span>
						</div>
						
						<div class="form-group">
						Calle Estados Unidos, Urb. San Felipe, Dpto. 12, Jesús Maria. Precio: S/. 2800 (el pago es en soles) Precio: $ 850 (precio referencial) ac. 120 m2. Mantenimiento: S/. 217 soles, no incluye agua, solo áreas comunes, medidor de agua independiente. Departamento en alquiler en Jesús Maria, de aprox. 8 años de antigüedad, en muy buen estado. Edifici...</div>
						<div class=" row">
							<div class="col text-info">
								Publicado hace 22 días
							</div>
							<div class="col text-right">
								<span class="btn btn-outline-primary text-uppercase"> CONTACtar</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 form-group card bg-white border border-warning border-left-0 border-right-0 border-bottom-0 border-height-4 p-0">
				<div class="row m-0">
					<div class="col-4 p-0">
						<img class="img-responsive w-100" src="https://img10.naventcdn.com/avisos/11/00/51/69/89/86/1200x1200/49271197.jpg">
						<div class="  p-2 ">
							<h5 class="">S/ 3,300</h5>
						</div>
					</div>
					<div class="col-8 p-3">
						<div class="form-group">
							<a href="<?=base_url();?>propiedades/alquiler/1" class="text-primary font-weight-bold text-uppercase ">Alquiler Departamento Triplex Lince Todo Incluido</a><br>
							<span><span class="fa fa-map-marker"></span>Jesús María, Lima</span>
						</div>
						
						<div class="form-group">
						Calle Estados Unidos, Urb. San Felipe, Dpto. 12, Jesús Maria. Precio: S/. 2800 (el pago es en soles) Precio: $ 850 (precio referencial) ac. 120 m2. Mantenimiento: S/. 217 soles, no incluye agua, solo áreas comunes, medidor de agua independiente. Departamento en alquiler en Jesús Maria, de aprox. 8 años de antigüedad, en muy buen estado. Edifici...</div>
						<div class=" row">
							<div class="col text-info">
								Publicado hace 22 días
							</div>
							<div class="col text-right">
								<span class="btn btn-outline-primary text-uppercase"> CONTACtar</span>
							</div>
						</div>
					</div>
				</div>
			</div> -->
		</div>
	</div>
</div>
</div>