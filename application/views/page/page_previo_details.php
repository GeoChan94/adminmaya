<div class="container ">
	<div class="row">
		<div class="col-md-8 col-xs-8 text-capitalize">
			<div class="row m-0">
				<div class="col-md-12 card p-2 bg-white">
					<div class="row m-0">
						<div class="col-auto">
							<span class="fa fa-arrow-left fa-2x"></span>
						</div>
						<div class="col">
							<h4 class="text-dark"> <?=$previo['titulo'];?></h4>
							<span class="fa fa-map-marker"></span> <?=$previo['provincia_previo_id_provincia'];?>
						</div>
					</div>
				</div>
				<div class="col-md-12 card mt-2 pt-3 pb-3">
					<nav>
					  <div class="nav nav-tabs" id="nav-tab" role="tablist">
					    <a class="nav-item nav-link active" id="nav-galeria-tab" data-toggle="tab" href="#nav-galeria" role="tab" aria-controls="nav-galeria" aria-selected="true">Galeria</a>
					    <?php if (!empty(@$previo['uri_video'])): ?>
					    	<a class="nav-item nav-link" id="nav-video-tab" data-toggle="tab" href="#nav-video" role="tab" aria-controls="nav-video" aria-selected="false">Video</a>
					    <?php endif ?>
					  </div>
					</nav>
					<div class="tab-content" id="nav-tabContent">
					  <div class="tab-pane fade show active" id="nav-galeria" role="tabpanel" aria-labelledby="nav-galeria-tab">
					  	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
						  <ol class="carousel-indicators">
						  	<?php foreach ($galeria as $key => $value): ?>
						  		<li data-target="#carouselExampleIndicators" data-slide-to="<?=$key;?>" class="<?=($key==0?'active':'')?>"></li>
						  	<?php endforeach ?>
						  </ol>
						  <div class="carousel-inner div-carousel text-center">
						  	<?php foreach ($galeria as $key => $value): ?>
						  		<div class="carousel-item h-100 <?=($key==0?'active':'')?>">
							      <img class="h-100" src="<?=$value;?>" alt="First slide">
							    </div>
						  	<?php endforeach ?>
						    
						  </div>
						  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					  </div>
					  <div class="tab-pane fade" id="nav-video" role="tabpanel" aria-labelledby="nav-video-tab">
					  	<div class="row mt-2">
					  		<iframe width="100%" height="315" src="<?='https://www.youtube.com/embed/'.$previo['uri_video'];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					  	</div>
					  </div>
					</div>





					
					
				
					<!-- <div class="row m-0 text-center mt-2 ">
						<div class="col text-uppercase"> <span class="btn btn-success">quiero que me llamen</span></div>
						<div class="col text-uppercase"> <span class="btn btn-success">enviar consulta</span></div>
						<div class="col text-uppercase"> <span class="btn btn-success">agendar visita</span></div>
					</div> -->
				</div>
				<div class="col-md-12 card mt-2 pt-3 pb-3">
					<h4 class="text-dark text-capitalize">ubicacion</h4>
					<span><span class="fa fa-map-marker"></span> <?=$previo['provincia_previo_id_provincia'];?></span>
					<!--
					<img class="figure-img img-fluid" src="https://programacion.net/files/article/20170405040453_mapa.jpg">
					-->
						<div id='map' style='width: 100%; height: 35em;'></div>
				</div>
				<div class="row m-0 w-100 mt-2 ">
					<div class="col-md-6 pl-0 ">
						<div class="card   p-3 text-capitalize">
							<h4 class="text-dark">Datos principales</h4>
							<!-- <div>mantenimiento: <?=$previo['mantenimiento'];?></div> -->
							<div>tipo: <?=$previo['tipo_previo_id_tipo_previo'];?></div>
							<div>forma pago: <?=$previo['tipo_pago_id_tipo_pago'];?></div>
							<div>modalidad: <?=$previo['modalidad_previo_id_modalidad_previo'];?></div>
							<div>documento:  <?=$previo['tipo_documento_previo_id_tipo_documento_previo'];?></div>
							<?php if (!empty($servicios)): ?>
								<div>Servicios:  <?php foreach($servicios as $key => $value){ echo $value['nombre'].(count($servicios)!=$key+1?', ':' '); }?></div>
							<?php endif ?>
							
							<div>Ciudad: <?=$previo['provincia_previo_id_provincia'];?></div>
							<?php if (!empty(@$caracteristicas[0]['orientacion'])): ?>
								<div>orientacion: <?=@$caracteristicas[0]['orientacion'];?></div>
							<?php endif ?>
							<?php if (!empty(@$caracteristicas[0]['estacionamientos'])): ?>
								<div>garaje: <?=@$caracteristicas[0]['estacionamientos'];?></div>
							<?php endif ?>
							<?php if (!empty(@$caracteristicas[0]['antiguedad'])): ?>
								<div>antiguedad: <?=@$caracteristicas[0]['antiguedad'];?></div>
							<?php endif ?>

							
							
							
							
						</div>
						
						
					</div>
					<div class="col-md-6 pr-0">
						<div class="card   p-3">
							<h4 class="text-dark">Descripción</h4>
							<p><?=$previo['descripcion_previo'];?></p>
						</div>
						
					</div>
				</div>
				
			</div>
		</div>
		<div class="col-md-4 col-xs-4 ">
			<div style="position: sticky; top: 1rem;">
			<div class="row m-0">
				<div class="col-md-12 card  text-left p-3">
					<h5 class="text-capitalize">Precio de <?=$previo['modalidad_previo_id_modalidad_previo'];?></h5>
					<h3 class="text-dark"><b>$ <?=$previo['precio'];?></b></h3>
					<hr>
					<div class="row m-0 text-center">
						<div class="col-md-6">
							<div class="row m-0">
								<div class="col-md-12 mb-1">
									Area
									<h5 class="text-dark"><?=$previo['area'];?> m2</h5>
									
								</div>
								<div class="col-md-12 mb-1">
									Medida frente 
									<h5 class="text-dark"><?=$previo['medida_frente'];?> m</h5>
									
								</div>
								<div class="col-md-12 mb-1">
									Medida fondo
									<h5 class="text-dark"><?=$previo['medida_fondo'];?> m</h5>
									 
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row m-0">
								<?php if (!empty(@$caracteristicas[0]['banios'])): ?>
									<div class="col-md-12 mb-1">
										Baños 
										<h5 class="text-dark"><?=@$caracteristicas[0]['banios'];?></h5>
										
									</div>
								<?php endif ?>
								<?php if (!empty(@$caracteristicas[0]['dormitorios'])): ?>
									<div class="col-md-12 mb-1">
										Dormitorios
										<h5 class="text-dark"><?=@$caracteristicas[0]['dormitorios'];?></h5> 
									</div>
								<?php endif ?>
								<?php if (!empty(@$caracteristicas[0]['pisos'])): ?>
									<div class="col-md-12 mb-1">
										Pisos
										<h5 class="text-dark"><?=@$caracteristicas[0]['pisos'];?></h5>
									</div>
								<?php endif ?>

								
								
								
							</div>
						</div>
						
						
					</div>
				</div>
				
					
				<div class="col-md-12 card mt-3 p-3 text-capitalize">
					<h5 class="text-dark">¡Contacta al anunciante!</h5>
					<div class="form-row">
						<div class="col-md-12 mb-3">
					      <label for="validationTooltip01">Mensaje</label>
					      <textarea class="form-control" id="mensaje" rows="3" required>Hola, vi este inmueble y quiero que me contacten. Gracias.</textarea>
					    </div>
					    <div class="col-md-12 mb-3">
					      <!-- <label for="validationTooltip01">Nombre</label> -->
					      <input type="text" class="form-control" id="nombre" placeholder="Nombre" value="" required>
					    </div>
					    <div class="col-md-12 mb-3">
					      <!-- <label for="validationTooltip02">Correo</label> -->
					      <input type="text" class="form-control" id="correo" placeholder="Correo" value="" required>
					    </div>
					    <div class="col-md-12 mb-3">
					      <!-- <label for="validationTooltipUsername">Telefono</label> -->
					      <div class="input-group">
					        <input type="text" class="form-control" id="celular" placeholder="Celular" required>

					      </div>
					    </div>
					  </div>
					  
					  <div class="btn btn-primary text-uppercase btn_mensaje" data-idprevio="<?=$previo['id_previo'];?>">contactar anunciante</div>
					  <div class="mt-2"><span class="msn_mensaje text-success"></span></div>
					  


				</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var arrayCoordenadas = "<?=@$previo['ubicacion_geografica']?>".split(",");
	mapboxgl.accessToken = 'pk.eyJ1IjoiaXZhbjcyMzkxIiwiYSI6ImNpb2xqZG1oOTAxcW11eG0xYzU2djlyY3YifQ.eR7IsqzlP6I6-h9lBDGfsw';
	var map = new mapboxgl.Map({
	    container: 'map',
	    style: 'mapbox://styles/mapbox/streets-v9',
	    center: [ parseFloat(arrayCoordenadas[0]), parseFloat(arrayCoordenadas[1]) ],
	    zoom: 16, 
	});

	var marker = new mapboxgl.Marker();
	marker.setLngLat([ parseFloat(arrayCoordenadas[0]), parseFloat(arrayCoordenadas[1]) ]);
    marker.addTo(map);
    

    
</script>