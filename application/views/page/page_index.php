<div class="w-100 position-relative">
	<img class="w-100" src="https://images5.alphacoders.com/365/thumb-1920-365641.jpg">
	<div class="row position-absolute t-0 p-5 text-center h-100 w-100">
		<div class="align-self-center w-100">
			<div class="col-12  font-weight-bold text-capitalize text-white" style="text-shadow: 0 0 5px rgba(0,0,0,.9); font-size: 2rem">
				<p>encuentra tu lugar</p>
				<br>
			</div>
			<div class="col-md-12 col-xs-12">
				<!-- <div class="row justify-content-md-center">
					<div class="col-xs-12 col-md-6">
						<div class="row text-light rounded border-0" style="background: rgba(0,0,0,.4);">
							<div class="col text-uppercase border border-light font-weight-bold p-2 rounded-left border-right-0">COMPRA</div>
							<div class="col text-uppercase border border-light font-weight-bold p-2">alquiler</div>
							<div class="col text-uppercase border border-light font-weight-bold p-2 rounded-right border-left-0">PROYECTOS</div>
						</div>
					</div>
					
				</div> -->
				<div class="row justify-content-md-center position-absolute w-100">
					<div class="col-xs-12 col-md-6 p-0">
						<br>
						<div class="row">
							<div class="col input-group mb-3  m-0">
								<select class="form-control col-4" id="select_modalidad_previo">

								  </select>
								  <div class="col-1 form-control">
								    <span >en</span>
								  </div>
								  <div class="col p-0 border-left-0 ">
								  	<!-- <input id="input-search" type="text" class="form-control" placeholder="busca por ejemplo en puno">
								  	<div class="div-result row bg-white m-0  rounded-bottom position-relative text-left"> -->
								  		<select class="form-control" id="distritos">

									    </select>
								  		
								  		
								  		
								  	<!-- </div> -->
								  </div>
							</div>


							<div class="col-auto pl-0">
								<div ><a class="btn btn-warning text-white font-weight-bold btn_buscar" >buscar</a></div>
							</div>
						  
						  
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
</div>


<div class="row m-0">
	<div class="col-12 text-center bg-light pt-2 pb-2">
		<div class="container">
			
		
			<div class="row">
				<div class="col-12 pt-3 pb-3">
					<h1 class="font-weight-bold">PROYECTOS INMOBILIARIOS</h1>
					<p>Estos son los últimos. Recuerda! que si tienes alguna consulta que quisieras compartir con nosotros, por favor envíanos un email o contactanos.</p>
				</div>
				<div class="row w-100 m-0 div_txt_proyecto"></div>
				<!-- <div class="col-md-4 col-12">
					<div class="card" >
					  <img class="card-img-top" src="https://vida-spyqpdxrgyld6rrkjib.netdna-ssl.com/wp-content/uploads/2015/03/JP-Torre-A-02.jpg" alt="Card image cap">
					  <div class="card-body">
					    <h5 class="card-title">Card title</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <a href="<?=base_url()?>propiedades/alquiler/1" class="btn btn-primary">ver detalles</a>
					  </div>
					</div>
				</div> -->
				
				
			</div>
		</div>
		
	</div>
	<div class="col-12 text-center bg-light pt-2 pb-2">
		<div class="container-fluid">
			
		
			<div class="row">
				<div class="col-12 pt-3 pb-3">
					<h1 class="font-weight-bold">PROPIEDADES EN VENTA </h1>
					<p>Estos son los últimos. Recuerda! que si tienes alguna consulta que quisieras compartir con nosotros, por favor envíanos un email o contactanos.</p>
				</div>
				<div class="row w-100 m-0 div_txt_venta mb-5"></div>				
				
			</div>
		</div>
		
	</div>
	<div class="col-12 text-center bg-light pt-2 pb-2">
		<div class="container">
			
		
			<div class="row">
				<div class="col-12 pt-3 pb-3">
					<h1 class="font-weight-bold">PROPIEDADES ALQUILER Y ANTICRESIS</h1>
					<p>Estos son los últimos. Recuerda! que si tienes alguna consulta que quisieras compartir con nosotros, por favor envíanos un email o contactanos.</p>
				</div>
				<div class="row w-100 m-0 div_txt_alquiler mb-5"></div>
				<!-- <div class="col-md-4 col-12">
					<div class="card" >
					  <img class="card-img-top" src="https://vida-spyqpdxrgyld6rrkjib.netdna-ssl.com/wp-content/uploads/2015/03/JP-Torre-A-02.jpg" alt="Card image cap">
					  <div class="card-body">
					    <h5 class="card-title">Card title</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <a href="<?=base_url()?>propiedades/alquiler/1" class="btn btn-primary">ver detalles</a>
					  </div>
					</div>
				</div> -->
				
				
				
			</div>
		</div>
		
	</div>
</div>


<style type="text/css">
	div.previo {
  /*margin: 10px;*/
  position: relative;
  overflow: hidden;
}

[data-in-stock]:after {
  position: absolute;
  top: 0;
  right: 0;
  z-index: 1;
 
  display: block;
  min-width: 250px; 
  color: #fff;
  background: #ff5722;
  /*padding: 6px 10px;*/
  
  text-align: center;
  text-transform: uppercase;
  font: 1em Arial;
  
      -webkit-transform: rotate(42deg) translateX(70px);
      -moz-transform: rotate(42deg) translateX(70px);
  transform: rotate(42deg) translateX(70px);
  height: 35px;
}

[data-in-stock="sold"]:after {
  content: url(assets/img/vendido.png);
}

</style>