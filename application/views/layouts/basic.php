<!doctype html>
<html lang="es" class="h-100">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title></title>
    <?php
        $this->load->view('recursos/css');
    ?>
</head>
<body class="bg-light h-100">
    <?php
        // $this->load->view('menu/header');
        // $this->load->view('menu/menu');
    ?> 
    <content data-spy="scroll" data-target="#main-menu" data-offset="0" class="h-100">
        <div class="row h-100 mh-100 m-0 bg-dark" >

            <!-- <div class="col-6 bg-secondary  h-100" >
                <div class="row  h-100">
                    <div class="col-12  align-self-center">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </div>
                </div>
                
                
            </div> -->
            <div class="col-12 col-md-4 offset-md-4 align-self-center rounded border pt-5 pb-5 bg-white">
                <?php   if(isset($_view) && $_view)
                    $this->load->view($_view);
                ?>
            </div>
        </div>
    
</content>  
<?php
    $this->load->view('recursos/js');
?> 
<?php
    // $this->load->view('footer/footer');
?>    
</body>

</html>