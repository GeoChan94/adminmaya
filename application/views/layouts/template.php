<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title></title>
    <?php
        $this->load->view('recursos/css');

    ?>
    
</head>
<body class="bg-light m-0 p-0">
    <?php
        // $this->load->view('menu/header');
        $this->load->view('menu/web/menu');
    ?> 
    <content data-spy="scroll" data-target="#main-menu" data-offset="0" >
	    <?php   if(isset($_view) && $_view)
            $this->load->view($_view);
        ?>
	</content>  
<?php
    $this->load->view('recursos/js');
?> 
<?php
    // $this->load->view('footer/footer');
?>    

</body>
</html>