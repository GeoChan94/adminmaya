<!DOCTYPE html>
<html lang="es">

	<head>
		<title></title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

	    <!-- CSS styles -->
	    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
	<?php
        $this->load->view('recursos/css');
    ?>
    <script src='https://api.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.js'></script>
	<link href='https://api.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.css' rel='stylesheet' />
	<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.3.0/mapbox-gl-geocoder.min.js'></script>
	<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.3.0/mapbox-gl-geocoder.css' type='text/css' />

	<?php
    $this->load->view('recursos/js');
	?> 
	</head>

	<body class="bg-light m-0 p-0">
	<?php
        // $this->load->view('menu/header');
        $this->load->view('menu/sadmin/menu');
    ?> 
    <content data-spy="scroll" data-target="#main-menu" data-offset="0" class="bg-light pt-4 pb-4 container">
		<?php	if(isset($_view) && $_view)
		    $this->load->view($_view);
		?>
	</content>  

	<?php
	    // $this->load->view('footer/footer');
	?> 
	</body>
</html>
