<div class="row ">
	<div class="col-md-12 bg-company font-weight-bold text-center p-5 text-uppercase text-company ">
		<h2 class="h1-lato">contacto</h2>
	</div>
	<div class="container">
		<div class="col-md-12 text-center p-3 font-weight-bold">
			<!-- <h4 class="text-capitalize">contactenos hinkas inmobiliaria</h4>	 -->
			<img src="<?=base_url();?>assets/img/logo.png" alt="" style="height: 100px;">
		</div>
		<div class="row m-0">
			<div class="col-md-6  bg-company h-100 pt-4 pb-4">
				<p class="h1-lato text-company">COMUNÍCATE CON NOSOTROS </p>
				<p>Escríbenos y ponte en contacto con nuestro equipo</p>
				<p>
					<span class="font-weight-bold">Oficina</span><br>
					<span class="fa fa-1x fa-map-marker text-company " ></span> Jr. Tacna Nº 370 - cercado Puno
				</p>
				<p>
					<span class="font-weight-bold">Celular</span><br>
					<span class="fa fa-mobile  text-company"></span> 998 08077 - 934 543 876
				</p>
				<p>
					<span class="font-weight-bold">Correo electronico</span><br>
					<span class="fa fa-envelope  text-company"></span> consultas@hinkas.pe
				</p>
				<p>Contamos con un equipo capacitado, comprometido y entusiasta que atenderá tus consultas y requerimientos otorgándote soluciones eficientes y oportunas.</p>
				<p>También te atendemos a través de nuestras redes sociales:</p>
				<a href="https://www.facebook.com/hinkas.pe/" target="_blank"><span class="fa  fa-2x fa-facebook-official text-primary"></span></a> @hinkas.pe
				
			</div>

			<div class="col-md-6 pt-4 pb-4">
				<p class="h1-lato text-company">GOOGLEMAPS</p>
				<!-- <iframe src="https://www.google.com/maps/d/embed?mid=1wi-SbNFqnjyNe0duncLHec04jCyy9ph8" width="640" height="480"></iframe> -->
				<iframe src="https://www.google.com/maps/d/u/0/embed?mid=1wi-SbNFqnjyNe0duncLHec04jCyy9ph8" width="640" height="400"></iframe>
				
				
			</div>
			
			
		</div>
		
	</div>
</div>