<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title></title>
    <?php
        $this->load->view('recursos/css');

    ?>
    <style type="text/css">
        .div-card-img{
            height: 300px;
        }
        @media only screen and (max-width: 767px) { 
            .logo_index{
                height: 40px !important;
            }
            .navbar-toggler {
                background-color: #ed841d !important;
            }
            #main-menu .navbar-collapse{
                background: #ffffffbd !important;
            }
            #main-menu .navbar-collapse .nav-item{
                padding: 5px !important;
                border-bottom: solid !important;
                border-color: #ed841d8a !important;
                border-width: 1px !important;
            }
            

                
        }
        
    </style>
    
</head>
<body class="bg-light m-0 p-0">
    <?php
        // $this->load->view('menu/header');
        $this->load->view('menu/web/menu_index');
    ?> 
    <content data-spy="scroll" data-target="#main-menu" data-offset="0" >
	    <?php
	        $this->load->view('page/page_index');
	    ?>
	</content>  
<?php
    $this->load->view('recursos/js');
?> 
<?php
    // $this->load->view('footer/footer');
?>    
<script type="text/javascript">
    
    var modalidad_previo = JSON.parse(`<?php echo json_encode(@$modalidad_previo);?>`);
    console.log('modalidad_previo',modalidad_previo);
    var modaldad_previo_text='';
    modalidad_previo.forEach(function(element,index){
        if (index==0) {
            modaldad_previo_text+='<option class="text-capitalize" value="'+element.nombre+'" selected>'+element.nombre+'</option>';
        }else{
            modaldad_previo_text+='<option class="text-capitalize" value="'+element.nombre+'">'+element.nombre+'</option>';
        }
        
    });
    $('#select_modalidad_previo').html(modaldad_previo_text);
    var pronvincias_previos = JSON.parse(`<?php echo json_encode(@$pronvincias_previos);?>`);
    console.log('pronvincias_previos',pronvincias_previos);
    var distritos_previo_text='';
    pronvincias_previos.forEach(function(element,index){
        if (index==0) {
            distritos_previo_text+='<option class="text-capitalize" value="'+reemplazar_espacio(element.nombre)+'" selected>'+element.nombre+'</option>';
        }else{
            distritos_previo_text+='<option class="text-capitalize" value="'+element.nombre+'">'+element.nombre+'</option>';
        }
    });
    console.log(distritos_previo_text);

    $('#distritos').html(distritos_previo_text);
    // 
    var modalidad=reemplazar_espacio(document.getElementById("select_modalidad_previo").value);
    var distritos=reemplazar_espacio(document.getElementById("distritos").value);

    $('.btn_buscar').prop('href','<?=base_url();?>buscar/'+modalidad.toLowerCase()+'/'+distritos.toLowerCase());
    $(document).on('change','#select_modalidad_previo',function(){
        console.log($(this).val());
        modalidad=reemplazar_espacio($(this).val());
        $('.btn_buscar').prop('href','<?=base_url();?>buscar/'+modalidad.toLowerCase()+'/'+distritos.toLowerCase());
    });
    $(document).on('change','#distritos',function(){
        console.log($(this).val());
        distritos=reemplazar_espacio($(this).val());
        $('.btn_buscar').prop('href','<?=base_url();?>buscar/'+modalidad.toLowerCase()+'/'+distritos.toLowerCase());
    });

    
    // 

    //
    var previo_proyecto = JSON.parse(`<?php echo json_encode(@$previo_proyecto);?>`);
    console.log('previo_proyecto',previo_proyecto);
    var txt_previo_proyecto='';
    $.each(previo_proyecto, function( index, value ) {
        txt_previo_proyecto+=''+
                '<div class="col-md-3 col-12 mb-3">'+
                    '<div class="card previo" data-in-stock="'+(value.state==1?'sold':'')+'">'+
                        
                            '<img class="card-img-top  div-card-img " src="'+value.uri_imagen_destacada+'" alt="Card image cap">'+
                       
                      '<div class="card-body">'+
                        '<h5 class="card-title text-left">'+
                            '<a class="text-decoration-none text-dark font-weight-bold " href="'+value.uri_previo+'" target="'+'_blank'+'">'+value.titulo+'</a>'+
                        '</h5>'+
                        '<p class="card-text text-muted text-left">'+value.descripcion_previo+'</p>'+
                        '<p class="card-text text-danger font-weight-bold "> $ '+value.precio+'</p>'+
                        
                        '<a href="'+value.uri_previo+'" class="btn btn-outline-danger btn-sm">ver detalles</a>'+
                      '</div>'+
                    '</div>'+
                '</div>';
    });
    $('.div_txt_proyecto').html(txt_previo_proyecto);
    // 


    // var previo_venta = JSON.parse(`<?php echo json_encode(@$previo_venta);?>`);
    // console.log('previo_venta',previo_venta);
    // var txt_previo_venta='';
    // $.each(previo_venta, function( index, value ) {
    //     txt_previo_venta+=''+
    //             '<div class="col-md-4 col-12 mb-3">'+
    //                 '<div class="card" >'+
    //                   '<img class="card-img-top div-card-img" src="'+value.uri_imagen_destacada+'" alt="Card image cap">'+
    //                   '<div class="card-body">'+
    //                     '<h5 class="card-title font-weight-bold">'+
    //                         '<a href="'+value.uri_previo+'" target="'+'_blank'+'">'+value.titulo+'</a>'+
    //                     '</h5>'+
    //                     '<p class="card-text">'+value.descripcion_previo+'</p>'+
    //                     '<p class="card-text text-danger font-weight-bold "> $ '+value.precio+'</p>'+
                        
    //                     '<a href="'+value.uri_previo+'" target="'+'_blank'+'" class="btn btn-outline-primary btn-sm">ver detalles</a>'+
    //                   '</div>'+
    //                 '</div>'+
    //             '</div>';
    // });
    // $('.div_txt_venta').html(txt_previo_venta);

    var previo_venta = JSON.parse(`<?php echo json_encode(@$previo_venta);?>`);
    console.log('previo_venta',previo_venta);
    var txt_previo_venta='';
    var container_venta='<div id="carusel_ventas" class="container-fluid carousel slide carousel-multi-item" data-ride="carousel">';
    var controls_venta='<div class="controls-top m-1">'+
                        '<a class="btn-floating" href="#carusel_ventas" data-slide="prev"><i class="fa fa-chevron-circle-left fa-2x"></i></a>'+
                        '<a class="btn-floating" href="#carusel_ventas" data-slide="next"><i class="fa fa-chevron-circle-right fa-2x"></i></a>'+
                      '</div>';
    var indicators_venta='<ol class="carousel-indicators">';
    var carusel_inner='<div class="carousel-inner d-flex" role="listbox">';
    var cont_venta=0;
    $.each(previo_venta, function( index, value ) {
        if (index==0) {
            txt_previo_venta+='<div class="row align-items-start carousel-item active">';
        }else if((index+1)%9==0){
            txt_previo_venta+='<div class="row align-items-start carousel-item">';
        }
        txt_previo_venta+=''+
                        '<div class="col-md-4 '+index+''+((index+2)%4)+' ">'+
                            '<div class="card mb-2 previo" data-in-stock="'+(value.state==1?'sold':'')+'">'+
                              '<img class="card-img-top div-card-img" src="'+value.uri_imagen_destacada+'" alt="Card image cap">'+
                              '<div class="card-body">'+
                                '<h5 class="card-title text-left">'+
                                    '<a class="text-decoration-none text-dark font-weight-bold " href="'+value.uri_previo+'" target="'+'_blank'+'">'+value.titulo+'</a>'+
                                '</h5>'+
                                '<p class="card-text text-muted text-left">'+value.descripcion_previo+'</p>'+
                                '<p class="card-text text-danger font-weight-bold "> $ '+value.precio+'</p>'+
                                '<a href="'+value.uri_previo+'" target="'+'_blank'+'" class="btn btn-outline-danger btn-sm">ver detalles</a>'+
                              '</div>'+
                            '</div>'+
                          '</div>';
        if ((index+2)%9==0||(index+1)==previo_venta.length) {
            txt_previo_venta+='</div>';
            indicators_venta+='<li data-target="#carusel_ventas" data-slide-to="'+(cont_venta)+'" class="'+(cont_venta==0?'active':'')+'"></li>';
            cont_venta++;
        }
    });
    $('.div_txt_venta').html(container_venta+controls_venta+indicators_venta+'</ol>'+carusel_inner+txt_previo_venta+'</div>'+'</div>'+'<div>');


    // 
    // var previo_alquiler = JSON.parse(`<?php echo json_encode(@$previo_alquiler);?>`);
    // console.log('previo_alquiler',previo_alquiler);
    // var txt_previo_alquiler='';
    // $.each(previo_alquiler, function( index, value ) {
    //     txt_previo_alquiler+=''+
    //             '<div class="col-md-4 col-12 mb-3">'+
    //                 '<div class="card" >'+
    //                   '<img class="card-img-top div-card-img" src="'+value.uri_imagen_destacada+'" alt="Card image cap">'+
    //                   '<div class="card-body">'+
    //                     '<h5 class="card-title text-left">'+
    //                                 '<a class="text-decoration-none text-dark font-weight-bold " href="'+value.uri_previo+'" target="'+'_blank'+'">'+value.titulo+'</a>'+
    //                             '</h5>'+
    //                     '<p class="card-text text-muted text-left">'+value.descripcion_previo+'</p>'+
    //                     '<p class="card-text text-danger font-weight-bold "> $ '+value.precio+'</p>'+
                        
    //                     '<a href="'+value.uri_previo+'" target="'+'_blank'+'" class="btn btn-outline-primary btn-sm">ver detalles</a>'+
    //                   '</div>'+
    //                 '</div>'+
    //             '</div>';
    // });
    // $('.div_txt_alquiler').html(txt_previo_alquiler);
    var previo_anticresis = JSON.parse(`<?php echo json_encode(@$previo_anticresis);?>`);
    console.log('previo_anticresis',previo_anticresis);

    var previo_alquiler = JSON.parse(`<?php echo json_encode(@$previo_alquiler);?>`);
    console.log('previo_alquiler',previo_alquiler);
    var txt_previo_alquiler='';
    var container_alquiler='<div id="carusel_alquiler" class="container-fluid carousel slide carousel-multi-item" data-ride="carousel">';
    var controls_alquiler='<div class="controls-top m-1">'+
                        '<a class="btn-floating" href="#carusel_alquiler" data-slide="prev"><i class="fa fa-chevron-circle-left fa-2x"></i></a>'+
                        '<a class="btn-floating" href="#carusel_alquiler" data-slide="next"><i class="fa fa-chevron-circle-right fa-2x"></i></a>'+
                      '</div>';
    var indicators_alquiler='<ol class="carousel-indicators">';
    var carusel_inner='<div class="carousel-inner" role="listbox">';
    var cont_alquiler=0;
    $.each(previo_alquiler, function( index, value ) {
        if (index==0) {
            txt_previo_alquiler+='<div class="row carousel-item active">';
        }else if((index+1)%4==0){
            txt_previo_alquiler+='<div class="row carousel-item">';
        }
        txt_previo_alquiler+=''+
                        '<div class="col-md-3 '+index+''+((index+2)%4)+' ">'+
                            '<div class="card mb-2 previo" data-in-stock="'+(value.state==1?'sold':'')+'">'+
                              '<img class="card-img-top div-card-img" src="'+value.uri_imagen_destacada+'" alt="Card image cap">'+
                              '<div class="card-body">'+
                                '<h5 class="card-title text-left">'+
                                    '<a class="text-decoration-none text-dark font-weight-bold " href="'+value.uri_previo+'" target="'+'_blank'+'">'+value.titulo+'</a>'+
                                '</h5>'+
                                '<p class="card-text text-muted text-left">'+value.descripcion_previo+'</p>'+
                                '<p class="card-text text-danger font-weight-bold "> $ '+value.precio+'</p>'+
                                '<a href="'+value.uri_previo+'" target="'+'_blank'+'" class="btn btn-outline-danger btn-sm">ver detalles</a>'+
                              '</div>'+
                            '</div>'+
                          '</div>';
        if ((index+2)%4==0||(index+1)==previo_alquiler.length) {
            txt_previo_alquiler+='</div>';
            indicators_alquiler+='<li data-target="#carusel_alquiler" data-slide-to="'+(cont_alquiler)+'" class="'+(cont_alquiler==0?'active':'')+'"></li>';
            cont_alquiler++;
        }
    });
    $('.div_txt_alquiler').html(container_alquiler+controls_alquiler+indicators_alquiler+'</ol>'+carusel_inner+txt_previo_alquiler+'</div>'+'</div>'+'<div>');


function reemplazar_espacio(text){
    return text.replace(/ /g, "-")

}
    

 


  


</script>
</body>
<style type="text/css">
    .div-result{
        max-height: 150px;
        overflow: auto;
    }
    .div-result-text{
        border-bottom: solid 1px #ddd;
        padding-top: .5%;
        padding-bottom: .5%;
    }
    .div-result-text a:hover{
        font-weight: bold;
    }
    /**/
    .carousel-item-next, .carousel-item-prev, .carousel-item.active {
    display: flex !important;
    }
    .carousel-multi-item .carousel-indicators {
        margin-bottom: -2em;
    }
    .carousel-multi-item .carousel-indicators li {
        height: 1rem;
        width: 1rem;
        max-width: 1rem;
        /*background-color: #4285f4;*/
        margin-bottom: -3.75rem;
        border-radius: 50%;
        border: #9E9E9E solid 1px;
    }
    .carousel-multi-item .carousel-indicators .active {
        height: 1.15rem;
        width: 1.15rem;
        max-width: 1.15rem;
        background-color: #4285f4;
        -webkit-border-radius: 50%;
        border: none;
        
    }
    
</style>
</html>