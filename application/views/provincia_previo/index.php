<div class="pull-right">
	<a href="<?php echo site_url('admin/provincia_previo/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Ciudad</th>
		<th>Nombre</th>
		<th>Actions</th>
    </tr>
	<?php foreach($provincia_previo as $p){ ?>
    <tr>
		<td><?php echo $p['id_provincia']; ?></td>
		<td><?php echo $p['nombre']; ?></td>
		<td>
            <a href="<?php echo site_url('admin/provincia_previo/edit/'.$p['id_provincia']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('admin/provincia_previo/remove/'.$p['id_provincia']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
