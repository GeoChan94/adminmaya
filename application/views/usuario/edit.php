<?php echo form_open('admin/usuario/edit/'.$usuario['id_usuario'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="tipo_usuario_id_tipo_usuario" class="col-md-4 control-label"><span class="text-danger">*</span>Tipo Usuario</label>
		<div class="col-md-8">
			<select name="tipo_usuario_id_tipo_usuario" class="form-control">
				<option value="">select tipo_usuario</option>
				<?php 
				foreach($all_tipo_usuario as $tipo_usuario)
				{
					$selected = ($tipo_usuario['id_tipo_usuario'] == $usuario['tipo_usuario_id_tipo_usuario']) ? ' selected="selected"' : "";

					echo '<option value="'.$tipo_usuario['id_tipo_usuario'].'" '.$selected.'>'.$tipo_usuario['descripcion'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('tipo_usuario_id_tipo_usuario');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="usuario" class="col-md-4 control-label"><span class="text-danger">*</span>Usuario</label>
		<div class="col-md-8">
			<input type="text" name="usuario" value="<?php echo ($this->input->post('usuario') ? $this->input->post('usuario') : $usuario['usuario']); ?>" class="form-control" id="usuario" />
			<span class="text-danger"><?php echo form_error('usuario');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="contrasenia" class="col-md-4 control-label"><span class="text-danger">*</span>Contraseña</label>
		<div class="col-md-8">
			<input type="text" name="contrasenia" value="<?php echo ($this->input->post('contrasenia') ? $this->input->post('contrasenia') : $usuario['contrasenia']); ?>" class="form-control" id="contrasenia" />
			<span class="text-danger"><?php echo form_error('contrasenia');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="nombres" class="col-md-4 control-label">nombres</label>
		<div class="col-md-8">
			<input type="text" name="nombres" value="<?php echo ($this->input->post('nombres') ? $this->input->post('nombres') : $usuario['nombres']); ?>" class="form-control" id="nombres" />
		</div>
	</div>
	<div class="form-group">
		<label for="apellidos" class="col-md-4 control-label">apellidos</label>
		<div class="col-md-8">
			<input type="text" name="apellidos" value="<?php echo ($this->input->post('apellidos') ? $this->input->post('apellidos') : $usuario['apellidos']); ?>" class="form-control" id="apellidos" />
		</div>
	</div>
	<div class="form-group">
		<label for="correo" class="col-md-4 control-label">correo</label>
		<div class="col-md-8">
			<input type="text" name="correo" value="<?php echo ($this->input->post('correo') ? $this->input->post('correo') : $usuario['correo']); ?>" class="form-control" id="correo" />
		</div>
	</div>
	<div class="form-group">
		<label for="celular" class="col-md-4 control-label">celular</label>
		<div class="col-md-8">
			<input type="text" name="celular" value="<?php echo ($this->input->post('celular') ? $this->input->post('celular') : $usuario['celular']); ?>" class="form-control" id="celular" />
		</div>
	</div>

	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>