<div class="pull-right">
	<a href="<?php echo site_url('admin/usuario/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Usuario</th>
		<th>Tipo Usuario Id Tipo Usuario</th>
		<th>Usuario</th>
		<th>Contraseña</th>
		<th>Actions</th>
    </tr>
	<?php foreach($usuario as $u){ ?>
    <tr>
		<td><?php echo $u['id_usuario']; ?></td>
		<td><?php echo $u['tipo_usuario']; ?></td>
		<td><?php echo $u['usuario']; ?></td>
		<td><?php echo $u['contrasenia']; ?></td>
		<td>
            <a href="<?php echo site_url('admin/usuario/edit/'.$u['id_usuario']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('admin/usuario/remove/'.$u['id_usuario']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
