<div class="pull-right">
	<a href="<?php echo site_url('admin/tipo_pago/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Tipo Pago</th>
		<th>Nombre</th>
		<th>Descripcion</th>
		<th>Actions</th>
    </tr>
	<?php foreach($tipo_pago as $t){ ?>
    <tr>
		<td><?php echo $t['id_tipo_pago']; ?></td>
		<td><?php echo $t['nombre']; ?></td>
		<td><?php echo $t['descripcion']; ?></td>
		<td>
            <a href="<?php echo site_url('admin/tipo_pago/edit/'.$t['id_tipo_pago']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('admin/tipo_pago/remove/'.$t['id_tipo_pago']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
