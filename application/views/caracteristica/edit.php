<?php echo form_open('admin/caracteristica/edit/'.$caracteristica['id_caracteristica'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="pisos" class="col-md-4 control-label">Pisos</label>
		<div class="col-md-8">
			<input type="text" name="pisos" value="<?php echo ($this->input->post('pisos') ? $this->input->post('pisos') : $caracteristica['pisos']); ?>" class="form-control" id="pisos" />
		</div>
	</div>
	<div class="form-group">
		<label for="orientacion" class="col-md-4 control-label">Orientacion</label>
		<div class="col-md-8">
			<input type="text" name="orientacion" value="<?php echo ($this->input->post('orientacion') ? $this->input->post('orientacion') : $caracteristica['orientacion']); ?>" class="form-control" id="orientacion" />
			<span class="text-danger"><?php echo form_error('orientacion');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="estacionamientos" class="col-md-4 control-label">Garaje</label>
		<div class="col-md-8">
			<input type="text" name="estacionamientos" value="<?php echo ($this->input->post('estacionamientos') ? $this->input->post('estacionamientos') : $caracteristica['estacionamientos']); ?>" class="form-control" id="estacionamientos" />
		</div>
	</div>
	<div class="form-group">
		<label for="antiguedad" class="col-md-4 control-label">Antiguedad</label>
		<div class="col-md-8">
			<input type="text" name="antiguedad" value="<?php echo ($this->input->post('antiguedad') ? $this->input->post('antiguedad') : $caracteristica['antiguedad']); ?>" class="form-control" id="antiguedad" />
		</div>
	</div>
	<div class="form-group">
		<label for="banios" class="col-md-4 control-label">Baños</label>
		<div class="col-md-8">
			<input type="text" name="banios" value="<?php echo ($this->input->post('banios') ? $this->input->post('banios') : $caracteristica['banios']); ?>" class="form-control" id="banios" />
		</div>
	</div>
	<div class="form-group">
		<label for="dormitorios" class="col-md-4 control-label">Dormitorios</label>
		<div class="col-md-8">
			<input type="text" name="dormitorios" value="<?php echo ($this->input->post('dormitorios') ? $this->input->post('dormitorios') : $caracteristica['dormitorios']); ?>" class="form-control" id="dormitorios" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>