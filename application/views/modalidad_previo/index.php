<div class="pull-right">
	<a href="<?php echo site_url('admin/modalidad_previo/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Modalidad Previo</th>
		<th>Nombre</th>
		<th>Descripcion</th>
		<th>Actions</th>
    </tr>
	<?php foreach($modalidad_previo as $m){ ?>
    <tr>
		<td><?php echo $m['id_modalidad_previo']; ?></td>
		<td><?php echo $m['nombre']; ?></td>
		<td><?php echo $m['descripcion']; ?></td>
		<td>
            <a href="<?php echo site_url('admin/modalidad_previo/edit/'.$m['id_modalidad_previo']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('admin/modalidad_previo/remove/'.$m['id_modalidad_previo']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
