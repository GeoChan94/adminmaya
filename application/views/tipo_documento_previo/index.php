<div class="pull-right">
	<a href="<?php echo site_url('admin/tipo_documento_previo/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Tipo Documento Previo</th>
		<th>Nombre</th>
		<th>Descripcion</th>
		<th>Actions</th>
    </tr>
	<?php foreach($tipo_documento_previo as $t){ ?>
    <tr>
		<td><?php echo $t['id_tipo_documento_previo']; ?></td>
		<td><?php echo $t['nombre']; ?></td>
		<td><?php echo $t['descripcion']; ?></td>
		<td>
            <a href="<?php echo site_url('admin/tipo_documento_previo/edit/'.$t['id_tipo_documento_previo']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('admin/tipo_documento_previo/remove/'.$t['id_tipo_documento_previo']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
