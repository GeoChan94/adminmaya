<div class="pull-right">
	<a href="<?php echo site_url('admin/previo/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Nº</th>
		<th>Titulo</th>
		<th>Area</th>
		<th>Precio</th>
		<th>Actions</th>
    </tr>
	<?php 
	if (!empty($previo)) {
		foreach($previo as $p){ ?>

			
		<tr>
			<td><?php echo $p['id_previo']; ?></td>
			<td>
				<a class="font-weight-bold text-black" href="<?php echo $p['uri_previo']; ?>" target='_blank'><?php echo $p['titulo']; ?></a><br>
				<span class="text-primary "><?php echo $p['modalidad_previo_id_modalidad_previo']; ?></span>
				<span class="text-secondary "><?php echo $p['provincia_previo_id_provincia']; ?></span>
				<span class="text-secondary ">/<?php echo $p['nombres']; ?></span>
				<span class="text-secondary ">[<?php echo $p['usuario'];?>]</span>
				
			</td>
			<td><?php echo $p['area']; ?></td>
			<td>$ <?php echo $p['precio']; ?></td>
			<td>
				<a href="<?php echo ($p['state']==1?site_url('admin/previo/desvender/'.$p['id_previo']):site_url('admin/previo/vender/'.$p['id_previo'])); ?>" class="btn btn-sm <?php echo($p['state']==1?'btn-success':'btn-outline-success');?> btn-xs" title="<?php echo($p['state']==1?'Vendido':'No vendido');?>" data-toggle="tooltip"><span class="fa fa-dollar" ></span></a> 

				<a href="<?php echo(!empty($p['mensaje'])? site_url('admin/mensaje/'.$p['id_previo']):'#'); ?>" class="btn btn-sm btn-success btn-xs" title="Mensajes" data-toggle="tooltip"><span class="fa fa-envelope" ></span> <span class="badge badge-light"><?=$p['mensaje'];?></span></a> 

				<a href="<?php echo site_url('admin/propietario/'.$p['id_previo']); ?>" class="btn btn-sm btn-success btn-xs" title="Datos propietario" data-toggle="tooltip"><span class="fa fa-user" ></span></a> 
				<a href="<?php echo site_url('admin/previo_has_servicio/'.$p['id_previo']); ?>" class="btn btn-sm btn-warning btn-xs" title="agregar servicios" data-toggle="tooltip"><span class="fa fa-shower" ></span></a> 
				<a href="<?php echo site_url('admin/caracteristica/'.$p['id_previo']); ?>" class="btn btn-sm btn-primary btn-xs" title="agregar caracteristicas" data-toggle="tooltip"><span class="fa fa-list"></span></a> 
				<a href="<?php echo site_url('admin/galeria/add/'.$p['id_previo']); ?>" class="btn btn-sm btn-success btn-xs" title="agregar galeria" data-toggle="tooltip"><span class="fa fa-photo"></span></a> 
	            <a href="<?php echo site_url('admin/previo/edit/'.$p['id_previo']); ?>" class="btn btn-sm btn-info btn-xs" title="editar previo" data-toggle="tooltip"><span class="fa fa-pencil"></span></a> 
	            <a href="<?php echo site_url('admin/previo/remove/'.$p['id_previo']); ?>" class="btn btn-sm btn-danger btn-xs" title="eliminar previo" data-toggle="tooltip"><span class="fa fa-close"></span></a>
	        </td>
	    </tr>
		<?php } 
	}
	?>
    
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
<script type="text/javascript">
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>