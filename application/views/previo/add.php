<?php echo form_open('admin/previo/add',array("class"=>"form-horizontal","id"=>"_formGaleria")); ?>

	
	<div class="form-group">
		<label for="titulo" class="col-md-4 control-label"><span class="text-danger">*</span>Titulo</label>
		<div class="col-md-8">
			<input type="text" name="titulo" value="<?php echo $this->input->post('titulo'); ?>" class="form-control" id="titulo" />
			<span class="text-danger"><?php echo form_error('titulo');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="uri_previo" class="col-md-4 control-label"><span class="text-danger">*</span>Uri Previo</label>
		<!-- <div class="col-md-8">
			<input type="text" name="uri_previo" value="<?php echo $this->input->post('uri_previo'); ?>" class="form-control" id="uri_previo" />
			<span class="text-danger"><?php echo form_error('uri_previo');?></span>
		</div> -->
		<div class="input-group col-md-8">
	        <input type="text" name="uri_previo" value="<?php echo $this->input->post('uri_previo'); ?>" class="form-control" id="uri_previo" />
	        <div class="input-group-prepend">
	          <div class="input-group-text">-#numero</div>
	        </div>
	        <span class="text-danger"><?php echo form_error('uri_previo');?></span>
	     </div>
	</div>
	<div class="form-group">
		<label for="ubicacion_geografica" class="col-md-4 control-label"><span class="text-danger">*</span>Ubicacion Geografica</label>
		<div class="col-md-8">
			<input type="text" name="ubicacion_geografica" value="<?php echo $this->input->post('ubicacion_geografica'); ?>" class="form-control" id="ubicacion_geografica" readOnly />
			<span class="text-danger"><?php echo form_error('ubicacion_geografica');?></span>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-8">
			<div id='map' style='width: 100%; height: 30em;'></div>
		</div>
	</div>
	<!-- <div class="form-group">
		<label for="mantenimiento" class="col-md-4 control-label">Mantenimiento</label>
		<div class="col-md-8">
			<input type="text" name="mantenimiento" value="<?php echo $this->input->post('mantenimiento'); ?>" class="form-control" id="mantenimiento" />
			<span class="text-danger"><?php echo form_error('mantenimiento');?></span>
		</div>
	</div> -->
	<div class="form-group">
		<label for="area" class="col-md-4 control-label"><span class="text-danger">*</span>Area</label>
		<div class="col-md-8">
			<input type="text" name="area" value="<?php echo $this->input->post('area'); ?>" class="form-control" id="area" />
			<span class="text-danger"><?php echo form_error('area');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="medida_frente" class="col-md-4 control-label"><span class="text-danger">*</span>Medida Frente</label>
		<div class="col-md-8">
			<input type="text" name="medida_frente" value="<?php echo $this->input->post('medida_frente'); ?>" class="form-control" id="medida_frente" />
			<span class="text-danger"><?php echo form_error('medida_frente');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="medida_fondo" class="col-md-4 control-label"><span class="text-danger">*</span>Medida Fondo</label>
		<div class="col-md-8">
			<input type="text" name="medida_fondo" value="<?php echo $this->input->post('medida_fondo'); ?>" class="form-control" id="medida_fondo" />
			<span class="text-danger"><?php echo form_error('medida_fondo');?></span>
		</div>
	</div>
	<!--
	<div class="form-group">
		<label for="uri_imagen_destacada" class="col-md-4 control-label"><span class="text-danger">*</span>Uri Imagen Destacada</label>
		<div class="col-md-8">
			<input type="text" name="uri_imagen_destacada" value="<?php echo $this->input->post('uri_imagen_destacada'); ?>" class="form-control" id="uri_imagen_destacada" />
			<span class="text-danger"><?php echo form_error('uri_imagen_destacada');?></span>
		</div>
	</div>
	-->
	<div class="form-group">
		<div class="col-md-6 mb-3">
			<label for="descripcion_categoria" class="col-md-12 pl-0 control-label">Seleccione una imágen destacada</label>
			<div class="col-md-12 pl-0">
				<div class="row">
					<div class="col-md-8">
						<div class="input-group">
							<div class="input-group-prepend">
							    <div class="btn btn-primary _fileUpload">
						        	<span><i class="fa fa-upload" aria-hidden="true"></i> Upload</span>
						            <input id="archivos" name="archivos"  type="file" multiple="false" class="file archivos upload" value="Seleccionar imágen...">
						        </div> 
							</div>
							<input type="text" name="uri_imagen" class="form-control" id="uri_imagen"  placeholder="Seleccione una imágen"/>
							<span class="text-danger"><?php echo form_error('uri_imagen');?></span>
						</div>
						
					</div>
					<div class="col-md-4">
						<div class="imagencargada text-left" id="imagencargada">
							
						</div>
					</div>
				</div>
		    </div>		
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<div class="img-thumbnail" id="divContainerGaleria" style="width: 100%; min-height: 20em;">

			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="uri_video" class="col-md-4 control-label">Url Video</label>
		<div class="col-md-8">
			<input type="text" name="uri_video" value="<?php echo $this->input->post('uri_video'); ?>" class="form-control" id="uri_video" />
		</div>
	</div>
	<div class="form-group">
		<label for="precio" class="col-md-4 control-label"><span class="text-danger">*</span>Precio</label>
		<div class="col-md-8">
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <span class="input-group-text">$</span>
			  </div>
			  <input type="text" name="precio" value="<?php echo $this->input->post('precio'); ?>" class="form-control" id="precio" />
			</div>
			<span class="text-danger"><?php echo form_error('precio');?></span>
		</div>
	</div>
	<!-- <div class="form-group">
		<label for="creacion" class="col-md-4 control-label"><span class="text-danger">*</span>Creacion</label>
		<div class="col-md-8">
			<input type="text" name="creacion" value="<?php echo $this->input->post('creacion'); ?>" class="form-control" id="creacion" />
			<span class="text-danger"><?php echo form_error('creacion');?></span>
		</div>
	</div> -->
	
	<div class="form-group">
		<label for="descripcion_previo" class="col-md-4 control-label"><span class="text-danger">*</span>Descripcion Previo</label>
		<div class="col-md-8">
			<textarea name="descripcion_previo" class="form-control" id="descripcion_previo"><?php echo $this->input->post('descripcion_previo'); ?></textarea>
			<span class="text-danger"><?php echo form_error('descripcion_previo');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="tipo_pago_id_tipo_pago" class="col-md-4 control-label"><span class="text-danger">*</span>Tipo Pago</label>
		<div class="col-md-8">
			<select name="tipo_pago_id_tipo_pago" class="form-control">
				<option value="">select tipo_pago</option>
				<?php 
				foreach($all_tipo_pago as $tipo_pago)
				{
					$selected = ($tipo_pago['id_tipo_pago'] == $this->input->post('tipo_pago_id_tipo_pago')) ? ' selected="selected"' : "";

					echo '<option value="'.$tipo_pago['id_tipo_pago'].'" '.$selected.'>'.$tipo_pago['nombre'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('tipo_pago_id_tipo_pago');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="tipo_previo_id_tipo_previo" class="col-md-4 control-label"><span class="text-danger">*</span>Tipo Previo</label>
		<div class="col-md-8">
			<select name="tipo_previo_id_tipo_previo" class="form-control">
				<option value="">select tipo_previo</option>
				<?php 
				foreach($all_tipo_previo as $tipo_previo)
				{
					$selected = ($tipo_previo['id_tipo_previo'] == $this->input->post('tipo_previo_id_tipo_previo')) ? ' selected="selected"' : "";

					echo '<option value="'.$tipo_previo['id_tipo_previo'].'" '.$selected.'>'.$tipo_previo['nombre'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('tipo_previo_id_tipo_previo');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="provincia_previo_id_provincia" class="col-md-4 control-label"><span class="text-danger">*</span>Ciudad Previo</label>
		<div class="col-md-8">
			<select name="provincia_previo_id_provincia" class="form-control">
				<option value="">select Ciudad_previo</option>
				<?php 
				foreach($all_provincia_previo as $provincia_previo)
				{
					$selected = ($provincia_previo['id_provincia'] == $this->input->post('provincia_previo_id_provincia')) ? ' selected="selected"' : "";

					echo '<option value="'.$provincia_previo['id_provincia'].'" '.$selected.'>'.$provincia_previo['nombre'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('provincia_previo_id_provincia');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="modalidad_previo_id_modalidad_previo" class="col-md-4 control-label"><span class="text-danger">*</span>Modalidad Previo</label>
		<div class="col-md-8">
			<select name="modalidad_previo_id_modalidad_previo" class="form-control">
				<option value="">select modalidad_previo</option>
				<?php 
				foreach($all_modalidad_previo as $modalidad_previo)
				{
					$selected = ($modalidad_previo['id_modalidad_previo'] == $this->input->post('modalidad_previo_id_modalidad_previo')) ? ' selected="selected"' : "";

					echo '<option value="'.$modalidad_previo['id_modalidad_previo'].'" '.$selected.'>'.$modalidad_previo['nombre'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('modalidad_previo_id_modalidad_previo');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="tipo_documento_previo_id_tipo_documento_previo" class="col-md-4 control-label"><span class="text-danger">*</span>Tipo Documento Previo</label>
		<div class="col-md-8">
			<select name="tipo_documento_previo_id_tipo_documento_previo" class="form-control">
				<option value="">select tipo_documento_previo</option>
				<?php 
				foreach($all_tipo_documento_previo as $tipo_documento_previo)
				{
					$selected = ($tipo_documento_previo['id_tipo_documento_previo'] == $this->input->post('tipo_documento_previo_id_tipo_documento_previo')) ? ' selected="selected"' : "";

					echo '<option value="'.$tipo_documento_previo['id_tipo_documento_previo'].'" '.$selected.'>'.$tipo_documento_previo['nombre'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('tipo_documento_previo_id_tipo_documento_previo');?></span>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(document).on('keyup', "#titulo", function(event) {
			event.preventDefault();
			var _title = $(this).val();
			$("#uri_previo").val( ((_title.trim()).replace(/ /g, "-").replace(/[/]/g, "-")).toLowerCase() );
		});
	});

	var markerSelected = null;
    var coordinatesSelectedLNG = -70.02788089671857;
    var coordinatesSelectedLAT = -15.84054059396469;
	var el = document.createElement('div');
  	el.className = 'marker';

	mapboxgl.accessToken = 'pk.eyJ1IjoiaXZhbjcyMzkxIiwiYSI6ImNpb2xqZG1oOTAxcW11eG0xYzU2djlyY3YifQ.eR7IsqzlP6I6-h9lBDGfsw';
	var map = new mapboxgl.Map({
		container: 'map',
		style: 'mapbox://styles/mapbox/streets-v9',
		//style: 'mapbox://styles/mapbox/satellite-v9',
		center:[-70.0280772,-15.8405466],
		zoom: 16,
	});
	//map.addControl(new mapboxgl.NavigationControl());
	var geocoder = new MapboxGeocoder({
    	accessToken: mapboxgl.accessToken
	});

	markerSelected = new mapboxgl.Marker(el).setLngLat([coordinatesSelectedLNG,coordinatesSelectedLAT]).addTo(map);

	map.addControl(geocoder);
	map.on('load', function() {
	    map.addSource('single-point', {
	        "type": "geojson",
	        "data": {
	            "type": "FeatureCollection",
	            "features": []
	        }
	    });

	    map.addLayer({
	        "id": "point",
	        "source": "single-point",
	        "type": "circle",
	        "paint": {
	            "circle-radius": 10,
	            "circle-color": "#007cbf"
	        }
	    });
	    geocoder.on('result', function(ev) {
	    	setCoordenadas(ev.result.geometry.coordinates[0]+","+ev.result.geometry.coordinates[1]);
	    	map.setCenter(ev.result.geometry.coordinates);
	        map.getSource('single-point').setData(ev.result.geometry);
	    });
	});
	
	map.on('click', function(e) {
		markerSelected.setLngLat([e.lngLat.lng,e.lngLat.lat]);
		setCoordenadas(e.lngLat.lng+","+e.lngLat.lat);
	});
	
	function setCoordenadas(_coordinates){
		document.getElementById("ubicacion_geografica").value= _coordinates;
	}

</script>

<style>
#geocoder-container > div {
    min-width:95%;
    margin-right:25%;
}
.marker {
  background-image: url('<?=site_url("assets/img/marker.png")?>');
  background-size: cover;
  width: 32px;
  height: 32px;
  border-radius: 50%;
  cursor: pointer;
}
</style>

<style type="text/css">
._fileUpload{
    position: relative;
    overflow: hidden;
}
._fileUpload input.upload{
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}	
</style>
<script type="text/javascript">
jQuery(document).ready(function($) {
	var fileExtension = "";
    $(':file').change(function(){
    	var typeFile  = "SLIDER";//$('select[name="type"] option:selected').val();
    	var titleFile = "";//$("#title").val();

        var file = $("#archivos")[0].files[0];
        var fileName = file.name;
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        var fileSize = file.size;
        var fileType = file.type;
        //showMessage("<span class='info'>Archivo para subir: "+fileName+", "+fileSize+" bytes.</span>");
        var formData = new FormData($("#_formGaleria")[0]);
        var message = ""; 
        $.ajax({
            url: '<?=site_url("assets/galeria/image.php")?>',  
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
                $("#imagencargada").empty();
                $("#imagencargada").append('<img src="<?=site_url('assets/img/loading.gif')?>" width="25%"/>');         
            },
            success: function(data){
                var rpta = $.parseJSON(data);
                //message = $("<span class='success'>La imagen ha subido correctamente.</span>");
                if(isImage(fileExtension)){
                    if ( rpta['rpta'] === 'success' ) {
                        //$("#uri_imagen").val(rpta['imagen']);
                        $("#imagen").prop("readonly",true);                        
                    }
                    //$("#divContainerGaleria").empty();
                    $("#divContainerGaleria").empty().append('<div class="float-left" style="position: relative; display: inline-block; text-align: center;"><img src="'+ "<?=site_url()?>" + rpta['uri_path'] + '" width="160" height="90" title="Imagen Representativa del Servicio" class="img-thumbnail"><input type="hidden" name="imagen[]" value="'+rpta['imagen']+'"><input type="hidden" name="tipo[]" value="'+typeFile+'"><input type="hidden" name="titulo_gallery[]" value="'+titleFile.trim()+'"><span class="btn btn-danger fa fa-times btn-sm btn-eliminar-imagen" data-uri="'+rpta['imagen']+'" data-option="1" data-id="" style="position: absolute; top: 8px;right: 8px; border: 0.1em solid #fff;" title="Eliminar"></span><span style="position: absolute; bottom: 5px;right: 4.8px; width:93%;text-align: center; color:white;background: rgba(0, 0, 0, 0.5); ">'+typeFile+'</span></div>'); 
                    $("#imagencargada").empty();

                    $("#uri_imagen").val(rpta['imagen']);
                    //$('#type').prop('selectedIndex',0);
                    //$("#title").val("").focus();
                }
            },
            error: function(){
                //message = $("<span class='error'>Ha ocurrido un error.</span>");
                //showMessage(message);
            }
        });
    });
 
    function isImage(extension){
        switch(extension.toLowerCase()) {
            case 'jpg': case 'gif': case 'png': case 'jpeg':
                return true;
            break;
            default:
                return false;
            break;
        }
    }

    $(document).on('click', '.btn-eliminar-imagen', function(event) {
    	event.preventDefault();
    	var htmlObject = $(this);
    	var id = $(this).data("id");
    	var uri = $(this).data("uri");
    	var option = $(this).data("option");
 		//console.log(id, uri,option);
 		
 		$.confirm({
	        content: function () {
	            var self = this;
	            return $.ajax({
	                url: "<?=site_url('galeria/remove')?>",
	                dataType: 'json',
	                method: 'post',
	                data: {id:id,uri:uri,option:option},
	            }).done(function (data) {
	                if ( data.response === "success" ) {
	                	htmlObject.parent().remove();
	                	$("#uri_imagen").val("");
	                	self.setTitle('Confirmación');
	                	self.setContent(data.message);	
	                }else{
	                	self.setTitle('Error');
	                	self.setContent(data.message);
	                }
	                
	            }).fail(function(e){
	            	self.setTitle('Error');
	                self.setContent(e.responseText);
	            });
	        },
	        buttons:{
	        	OK:{
	        		text:"Ok",
	        		btnClass:"btn-red",
	        		action:function(event){

	        		}
	        	}
	        },
	         theme: 'material'
	    });
 		//$(this).parent().remove();
    });
});
</script>