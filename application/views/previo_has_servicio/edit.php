<?php echo form_open('admin/previo_has_servicio/edit/'.$previo_has_servicio['id_previo_has_serviciocol'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="previo_id_previo" class="col-md-4 control-label"><span class="text-danger">*</span>Previo</label>
		<div class="col-md-8">
			<select name="previo_id_previo" class="form-control">
				<option value="">select previo</option>
				<?php 
				foreach($all_previo as $previo)
				{
					$selected = ($previo['id_previo'] == $previo_has_servicio['previo_id_previo']) ? ' selected="selected"' : "";

					echo '<option value="'.$previo['id_previo'].'" '.$selected.'>'.$previo['id_previo'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('previo_id_previo');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="servicio_id_servicio" class="col-md-4 control-label"><span class="text-danger">*</span>Servicio</label>
		<div class="col-md-8">
			<select name="servicio_id_servicio" class="form-control">
				<option value="">select servicio</option>
				<?php 
				foreach($all_servicio as $servicio)
				{
					$selected = ($servicio['id_servicio'] == $previo_has_servicio['servicio_id_servicio']) ? ' selected="selected"' : "";

					echo '<option value="'.$servicio['id_servicio'].'" '.$selected.'>'.$servicio['id_servicio'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('servicio_id_servicio');?></span>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>