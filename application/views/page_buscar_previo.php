<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title></title>
    <?php
        $this->load->view('recursos/css');
    ?>
</head>
<body class="bg-light m-0 p-0">
    <?php
        // $this->load->view('menu/header');
        $this->load->view('menu/web/menu');
    ?> 
    <content data-spy="scroll" data-target="#main-menu" data-offset="0" class="bg-light pt-4 pb-4">
	    <?php
	        $this->load->view('page/page_buscar_previo');
	    ?>
	</content>  
<?php
    $this->load->view('recursos/js');
?> 
<?php
    // $this->load->view('footer/footer');
?> 
    <script type="text/javascript">
        var modalidad = JSON.parse(`<?php echo json_encode(@$modalidad);?>`);
        console.log('modalidad',modalidad);
        var provincia = JSON.parse(`<?php echo json_encode(@$provincia);?>`);
        console.log('provincia',provincia);
        var previo = JSON.parse(`<?php echo json_encode(@$previo);?>`);
        console.log('previo',previo);
        txt_previo='';
        previo.forEach(function(element,index){
            txt_previo+=''+
            '<div class="col-12 form-group card bg-white border border-warning border-left-0 border-right-0 border-bottom-0 border-height-4 p-0">'+
                '<div class="row m-0">'+
                    '<div class="col-4 p-0">'+
                        '<img class="img-responsive w-100" src="'+element.uri_imagen_destacada+'">'+
                        '<div class="  p-2 ">'+
                            '<h5 class="">S/ '+element.precio+'</h5>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-8 p-3">'+
                        '<div class="form-group">'+
                            '<a href="'+element.uri_previo+'" class="text-primary font-weight-bold text-uppercase ">'+element.titulo+'</a><br>'+
                            '<span><span class="fa fa-map-marker"></span>Jesús María, Lima</span>'+
                        '</div>'+
                        
                        '<div class="form-group">'+
                        ' '+element.descripcion_previo+' </div>'+
                        '<div class=" row">'+
                            '<div class="col-md-12">'+element.tipo_previo_id_tipo_previo+'</div>'+
                            '<div class="col text-info">'+
                                'Publicado el '+element.creacion+
                            '</div>'+
                            '<div class="col text-right">'+
                                '<span class="btn btn-outline-primary text-uppercase"> CONTACtar</span>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>';
        });
        $('.div_previos').html(txt_previo);

    </script>   
</body>


</html>