
<h4 class="text-uppercase">Proyectos seleccionados </h4>
<span class=" btn btn-success btn_modal" data-modalidad='proyecto' >Agregar</span>
<span class="text-muted">Seleccione 3 Proyectos</span> 



<!-- <div class="input-group mb-3">
  <input type="text" class="form-control" id="id_proyecto" aria-describedby="basic-addon3" placeholder="#id de proyecto">
  <div class="input-group-prepend">
    <span class=" btn btn-success" id="btn_proyecto">Agregar</span>
  </div>
</div> -->
<div class="container-fluid ">
  <table class="table table-striped">
  <thead class="table-primary">
    <tr>
      <th >#</th>
      <th>nombre proyecto</th>
      <th>acciones</th>
    </tr>
  </thead>
  <tbody class="div_proyectos">
  </tbody>
</table>
</div>
<h4 class="text-uppercase">Propiedades en venta seleccionados</h4>
<span class=" btn btn-success btn_modal" data-modalidad='venta' >Agregar</span>
<span class="text-muted">Seleccione 3 Previos</span> 
<!-- <div class="input-group mb-3">
  <input type="text" class="form-control" id="id_venta" aria-describedby="basic-addon3" placeholder="#id de propiedad">
  <div class="input-group-prepend">
    <span class=" btn btn-success" id="btn_venta">Agregar</span>
  </div>
</div> -->
<div class="container-fluid ">
  <table class="table table-striped">
  <thead class="table-primary">
    <tr>
      <th>#</th>
      <th>nombre propiedad</th>
      <th>acciones</th>
    </tr>
  </thead>
  <tbody class="div_ventas">
  </tbody>
</table>
</div>

<h4 class="text-uppercase">Propiedades en alquiler y anticresis seleccionados</h4>
<span class=" btn btn-success btn_modal" data-modalidad='alquiler_anticresis' >Agregar</span>
<span class="text-muted">Seleccione 3 Previos</span> 
<!-- <div class="input-group mb-3">
  <input type="text" class="form-control" id="id_alquiler" aria-describedby="basic-addon3" placeholder="#id de propiedad">
  <div class="input-group-prepend">
    <span class=" btn btn-success" id="btn_alquiler">Agregar</span>
  </div>
</div> -->
<div class="container-fluid ">
  <table class="table table-striped">
  <thead class="table-primary">
    <tr>
      <th>#</th>
      <th>nombre propiedad</th>
      <th>acciones</th>
    </tr>
  </thead>
  <tbody class="div_alquiler">
  </tbody>
</table>
</div>



<!-- Modal -->
<div class="modal fade " id="modal_previos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body modal_body_previos">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">



//////////////////////////////////////
    
    var previo_proyecto = '';
    previo_proyecto = JSON.parse(`<?php echo json_encode(@$previo_proyecto);?>`);
    console.log('previo_proyecto',previo_proyecto);
    var proyectos_txt='';
    var previo_proyecto_filtrado='';
    if (previo_proyecto) {
      previo_proyecto_filtrado = previo_proyecto.filter(function(el){
        if (el['select_index']==1) {
          return el;
        }
        
      });
    }
    

    $.each(previo_proyecto_filtrado, function( index, value ) {
      proyectos_txt+=''+
                  '<tr>'+
                    '<td>'+value.id_previo+'</th>'+
                    '<td>'+(value.state==1?'<span class=" btn-success btn-sm fa fa-dollar"></span> ':'')+value.titulo+'</td>'+
                    '<td><span class="btn btn-sm btn-danger btn-xs " onclick="des_select_previo('+value.id_previo+')" data-idproyecto="'+value.id_previo+'" title="" data-toggle="tooltip" data-original-title="eliminar previo"><span class="fa fa-close"></span></span></td>'+
                  '</tr>';
      });
    // 
    $(".div_proyectos").html(proyectos_txt);

    /////////////////////////////
    $(".btn_modal").on( "click", function(){
      var modalidad=$(this).data('modalidad');
      $('#modal_previos').modal('show');
      switch(modalidad){
        case 'proyecto':
          mostrar_previos_modal(previo_proyecto);
          break;
        case 'venta':
          mostrar_previos_modal(previo_venta);
          break;
        case 'alquiler_anticresis':
          mostrar_previos_modal(previo_alquiler);
          break;
        default:
      } 
      
     
    });


    


    function mostrar_previos_modal(previos){
      var temp_proyectos_txt='';
      var previos_filtrado;
      $(".modal_body_previos").html('Sin lista de previos');
      $(".modal-title").html('Previos');
      if (previos) {
         previos_filtrado= previos.filter(function(el){
          if (el['select_index']==0) {
            return el;
          }
        });

        $.each(previos_filtrado, function( index, value ) {
          temp_proyectos_txt+=''+
                      '<tr>'+
                        '<td>'+value.id_previo+'</th>'+
                        '<td>'+(value.state==1?'<span class=" btn-success btn-sm fa fa-dollar"></span> ':'')+value.titulo+'</td>'+
                        '<td><span class="btn btn-sm btn-outline-success btn-xs btn_select_previo" onclick="select_previo('+value.id_previo+')" data-idprevio="'+value.id_previo+'" title="" data-toggle="tooltip" data-original-title="Agregar Proyecto"><span class="fa fa-check"></span></span></td>'+
                      '</tr>';
          });

      $(".modal_body_previos").html('<table class="table">'+temp_proyectos_txt+'</table>');
      $(".modal-title").html('Propiedades '+previos[0]['modalidad_previo_id_modalidad_previo']);

      }

      

    }
    function select_previo(elem){

      var id_previo=elem;

      $.ajax({
        data: {"id_previo" : id_previo,"select_index" : 1},
        type: "POST",
        url: '<?php echo base_url()?>admin/previo/edit_check_index/'+id_previo,
      })
       .done(function( data, textStatus, jqXHR ) {
           if ( console && console.log ) {
               console.log( "La solicitud se ha completado correctamente." );
               location.reload();
           }
       })
       .fail(function( jqXHR, textStatus, errorThrown ) {
           if ( console && console.log ) {
               console.log( "La solicitud a fallado: " +  textStatus);
           }
      });

    }
    function des_select_previo(elem){
      var id_previo=elem;

      $.ajax({
        data: {"id_previo" : id_previo,"select_index" : 0},
        type: "POST",
        url: '<?php echo base_url()?>admin/previo/edit_check_index/'+id_previo,
      })
       .done(function( data, textStatus, jqXHR ) {
           if ( console && console.log ) {
               console.log( "La solicitud se ha completado correctamente." );
               location.reload();
           }
       })
       .fail(function( jqXHR, textStatus, errorThrown ) {
           if ( console && console.log ) {
               console.log( "La solicitud a fallado: " +  textStatus);
           }
      });

    }
    // --------------------------------------------------------
    var previo_venta='';
    previo_venta = JSON.parse(`<?php echo json_encode(@$previo_venta);?>`);
    console.log('previo_venta',previo_venta);
    var ventas_txt='';
    var previo_venta_filtrado ='';
    if (previo_venta) {
      previo_venta_filtrado = previo_venta.filter(function(el){
        if (el['select_index']==1) {
          return el;
        }
        
      });
    }
    



    $.each(previo_venta_filtrado, function( index, value ) {
      ventas_txt+=''+
                  '<tr>'+
                    '<td>'+value.id_previo+'</th>'+
                    '<td>'+value.titulo+'</td>'+
                    '<td><span class="btn btn-sm btn-danger btn-xs " onclick="des_select_previo('+value.id_previo+')" data-idventa="'+value.id_previo+'" title="" data-toggle="tooltip" data-original-title="eliminar previo"><span class="fa fa-close"></span></span></td>'+
                  '</tr>';
      });
    $(".div_ventas").html(ventas_txt);
    // 



    // -----------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------
    var previo_alquiler='';
    previo_alquiler = JSON.parse(`<?php echo json_encode(@$previo_alquiler);?>`);
    console.log('previo_alquiler',previo_alquiler);
    var proyectos_txt='';
    var previo_alquiler_filtrado ='';
    if (previo_alquiler) {
      previo_alquiler_filtrado = previo_alquiler.filter(function(el){
        if (el['select_index']==1) {
          return el;
        }
      });
    }
    

    $.each(previo_alquiler_filtrado, function( index, value ) {
      proyectos_txt+=''+
                  '<tr>'+
                    '<td>'+value.id_previo+'</th>'+
                    '<td>'+value.titulo+'</td>'+
                    '<td><span class="btn btn-sm btn-danger btn-xs " onclick="des_select_previo('+value.id_previo+')" data-idalquiler="'+value.id_previo+'" title="" data-toggle="tooltip" data-original-title="eliminar previo"><span class="fa fa-close"></span></span></td>'+
                  '</tr>';
      });
    // 
    $(".div_alquiler").html(proyectos_txt);


    
    

</script>