<nav id="main-menu" class=" navbar  navbar-light navbar-expand-lg  menu-custom navbar-main" >
        <div class="container">

            <a class="navbar-brand p-0 " href="<?=base_url();?>">
                <img src="<?=base_url();?>assets/img/logo_menu.png" class="logo_index" alt="" style="height: 80px;">
                <!-- <span class="h1-lato">MAYA</span> -->
                <span class="font-weight-bold text-lowercase">
                 <!--//Hinkas-->
                </span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse " id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto nav nav-pills">
                    <li class="nav-item active">
                        <a data-item="" class="nav-link h1-lato " href="<?=base_url();?>"><span class="fa fa-home"></span> Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a data-item="#ofrecemos" class="nav-link h1-lato" href="<?=base_url();?>#ofrecemos">venta</a>
                    </li>
                    <li class="nav-item">
                        <a data-item="#ofrecemos" class="nav-link h1-lato" href="<?=base_url();?>#ofrecemos">alquiler</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a data-item="#nosotros" class="nav-link h1-lato" href="<?=base_url();?>#nosotros">nosotros</a>
                    </li> -->
                    
                    <li class="nav-item">
                        <a data-item="#contactenos" class="nav-link h1-lato " href="<?=base_url();?>contacto">Contactenos</a>
                    </li>
                     <!-- <li class="nav-item">
                        <a class="nav-link btn_login" href="#"> 
                            <span class=" border text-danger  border-danger rounded pt-1 pb-1 pr-3 pl-3 h1-lato">Entrar</span>
                        </a>
                      </li> -->
      
                </ul>
            </div>
        </div>

    </nav>