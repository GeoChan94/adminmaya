<?php echo form_open('admin/propietario/add/'.$previo['id_previo'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="nombres" class="col-md-4 control-label text-capitalize">nombres</label>
		<div class="col-md-8">
			<input type="text" name="nombres" value="<?php echo ($this->input->post('nombres') ? $this->input->post('nombres') : @$propietario['nombres']); ?>" class="form-control" id="nombres" />
		</div>
	</div>
	<div class="form-group">
		<label for="apellidos" class="col-md-4 control-label text-capitalize">apellidos</label>
		<div class="col-md-8">
			<input type="text" name="apellidos" value="<?php echo ($this->input->post('apellidos') ? $this->input->post('apellidos') : @$propietario['apellidos']); ?>" class="form-control" id="apellidos" />
		</div>
	</div>
	<div class="form-group">
		<label for="correo" class="col-md-4 control-label text-capitalize">correo</label>
		<div class="col-md-8">
			<input type="text" name="correo" value="<?php echo ($this->input->post('correo') ? $this->input->post('correo') : @$propietario['correo']); ?>" class="form-control" id="correo" />
		</div>
	</div>
	<div class="form-group">
		<label for="celular" class="col-md-4 control-label text-capitalize">celular</label>
		<div class="col-md-8">
			<input type="text" name="celular" value="<?php echo ($this->input->post('celular') ? $this->input->post('celular') : @$propietario['celular']); ?>" class="form-control" id="celular" />
		</div>
	</div>
	<div class="form-group">
		<label for="dni" class="col-md-4 control-label text-capitalize">dni</label>
		<div class="col-md-8">
			<input type="text" name="dni" value="<?php echo ($this->input->post('dni') ? $this->input->post('dni') : @$propietario['dni']); ?>" class="form-control" id="dni" />
		</div>
	</div>
	<div class="form-group">
		<label for="direccion" class="col-md-4 control-label text-capitalize">direccion</label>
		<div class="col-md-8">
			<input type="text" name="direccion" value="<?php echo ($this->input->post('direccion') ? $this->input->post('direccion') : @$propietario['direccion']); ?>" class="form-control" id="direccion" />
		</div>
	</div>
	<div class="form-group">
		<label for="comentarios" class="col-md-4 control-label text-capitalize">comentarios</label>
		<div class="col-md-8">
			<input type="text" name="comentarios" value="<?php echo ($this->input->post('comentarios') ? $this->input->post('comentarios') : @$propietario['comentarios']); ?>" class="form-control" id="comentarios" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>