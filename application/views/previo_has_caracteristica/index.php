<div class="pull-right">
	<a href="<?php echo site_url('admin/previo_has_caracteristica/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Previo Has Caracteristicacol</th>
		<th>Previo Id Previo</th>
		<th>Caracteristica Id Caracteristica</th>
		<th>Actions</th>
    </tr>
	<?php foreach($previo_has_caracteristica as $p){ ?>
    <tr>
		<td><?php echo $p['id_previo_has_caracteristicacol']; ?></td>
		<td><?php echo $p['previo_id_previo']; ?></td>
		<td><?php echo $p['caracteristica_id_caracteristica']; ?></td>
		<td>
            <a href="<?php echo site_url('admin/previo_has_caracteristica/edit/'.$p['id_previo_has_caracteristicacol']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('admin/previo_has_caracteristica/remove/'.$p['id_previo_has_caracteristicacol']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
