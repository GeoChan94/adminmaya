<?php echo form_open('admin/previo_has_caracteristica/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="previo_id_previo" class="col-md-4 control-label"><span class="text-danger">*</span>Previo</label>
		<div class="col-md-8">
			<select name="previo_id_previo" class="form-control">
				<option value="">select previo</option>
				<?php 
				foreach($all_previo as $previo)
				{
					$selected = ($previo['id_previo'] == $this->input->post('previo_id_previo')) ? ' selected="selected"' : "";

					echo '<option value="'.$previo['id_previo'].'" '.$selected.'>'.$previo['id_previo'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('previo_id_previo');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="caracteristica_id_caracteristica" class="col-md-4 control-label"><span class="text-danger">*</span>Caracteristica</label>
		<div class="col-md-8">
			<select name="caracteristica_id_caracteristica" class="form-control">
				<option value="">select caracteristica</option>
				<?php 
				foreach($all_caracteristica as $caracteristica)
				{
					$selected = ($caracteristica['id_caracteristica'] == $this->input->post('caracteristica_id_caracteristica')) ? ' selected="selected"' : "";

					echo '<option value="'.$caracteristica['id_caracteristica'].'" '.$selected.'>'.$caracteristica['id_caracteristica'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('caracteristica_id_caracteristica');?></span>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>