<div class="pull-right">
	<a href="<?php echo site_url('admin/servicio/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Servicio</th>
		<th>Nombre</th>
		<th>Descripcion</th>
		<th>Actions</th>
    </tr>
	<?php foreach($servicio as $s){ ?>
    <tr>
		<td><?php echo $s['id_servicio']; ?></td>
		<td><?php echo $s['nombre']; ?></td>
		<td><?php echo $s['descripcion']; ?></td>
		<td>
            <a href="<?php echo site_url('admin/servicio/edit/'.$s['id_servicio']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('admin/servicio/remove/'.$s['id_servicio']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
