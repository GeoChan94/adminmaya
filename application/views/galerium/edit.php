<?php echo form_open('admin/galerium/edit/'.$galerium['id_galeria'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="uri_galeria" class="col-md-4 control-label"><span class="text-danger">*</span>Uri Galeria</label>
		<div class="col-md-8">
			<input type="text" name="uri_galeria" value="<?php echo ($this->input->post('uri_galeria') ? $this->input->post('uri_galeria') : $galerium['uri_galeria']); ?>" class="form-control" id="uri_galeria" />
			<span class="text-danger"><?php echo form_error('uri_galeria');?></span>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>