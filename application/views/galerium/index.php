<div class="pull-right">
	<a href="<?php echo site_url('admin/galerium/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Galeria</th>
		<th>Uri Galeria</th>
		<th>Actions</th>
    </tr>
	<?php foreach($galeria as $g){ ?>
    <tr>
		<td><?php echo $g['id_galeria']; ?></td>
		<td><?php echo $g['uri_galeria']; ?></td>
		<td>
            <a href="<?php echo site_url('admin/galerium/edit/'.$g['id_galeria']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('admin/galerium/remove/'.$g['id_galeria']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
