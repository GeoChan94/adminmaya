<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-primary">GALERIA DE IMÁGENES PARA <?=mb_strtoupper(trim(@$predio['titulo']))?></h2>
			<?php  
				//echo json_encode($predio);
				//echo json_encode($archivos);
			?>
		</div>
	</div>
</div>

<?php echo form_open('admin/galeria/add/'.@$predio['id_previo'],array("class"=>"form-horizontal","id"=>"_formGaleria")); ?>

	<div class="box-body">
  		<div class="row clearfix">
			<div class="col-md-6">
				<label for="type" class="control-label"><span class="text-danger">*</span>Tipo de Imágen</label>
				<div class="form-group">
					<select name="type" class="form-control">
						<!--
						<option value="">seleccione...</option>
						-->
						<?php 
						$type_values = array(
							'IMAGEN'=>'Imágen Normal',
							'SLIDER'=>'Slider (Para la cabecera o imágen Representativa)',
							'THUMBNAIL'=>'Imágen Miniatura (Thumbnail)',
						);

						foreach($type_values as $value => $display_text)
						{
							$selected = ($value == $this->input->post('type')) ? ' selected="selected"' : "";

							echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
						} 
						?>
					</select>
					<span class="text-danger"><?php echo form_error('type');?></span>
				</div>
			</div>
			<!--
			<div class="col-md-6">
				<label for="title" class="control-label">Título para la imágen (Opcional)</label>
				<div class="form-group">
					<input type="text" name="title" value="<?php echo $this->input->post('title'); ?>" class="form-control" id="title"  autofocus/>
				</div>
			</div>
			-->
			<div class="col-md-6 mb-3">
				<label for="descripcion_categoria" class="col-md-12 pl-0 control-label">Seleccione una imágen</label>
				<div class="col-md-12 pl-0">
					<div class="row">
						<div class="col-md-8">
							<div class="input-group">
								<div class="input-group-prepend">
								    <div class="btn btn-primary _fileUpload">
							        	<span><i class="fa fa-upload" aria-hidden="true"></i> Upload</span>
							            <input id="archivos" name="archivos"  type="file" multiple="false" class="file archivos upload" value="Seleccionar imágen...">
							        </div> 
								</div>
								<input type="text" name="uri_imagen" class="form-control" id="uri_imagen"  placeholder="Seleccione una imágen"/>
							</div>
							
						</div>
						<div class="col-md-4">
							<div class="imagencargada text-left" id="imagencargada">
								
							</div>
						</div>
					</div>
			    </div>		
			</div>
			<div class="col-md-6"></div>
			<div class="col-md-12">
				<div class="img-thumbnail" id="divContainerGaleria" style="width: 100%; min-height: 20em;">
					<?php foreach ($archivos as $key => $value): ?>
						<div class="float-left" style="position: relative; display: inline-block; text-align: center;">
							<img src="<?=site_url('assets/galeria/imagenes/'.$value['uri_galeria'])?>" width="160" height="90" title="Imagen Representativa del Servicio" class="img-thumbnail">
							<input type="hidden" name="imagen[]" value="<?=$value['uri_galeria']?>">
							<input type="hidden" name="tipo[]" value="<?=$value['tipo']?>">
							<!--
							<input type="hidden" name="titulo[]" value="<?=@$value['title']?>">
							-->
							<span class="btn btn-danger fa fa-times btn-sm btn-eliminar-imagen" data-id="<?=$value['id_galeria']?>" data-uri="<?=$value['uri_galeria']?>" data-option="2" style="position: absolute; top: 8px;right: 8px; border: 0.1em solid #fff;" title="Eliminar"></span><span style="position: absolute; bottom: 5px;right: 4.8px; width:93%;text-align: center; color:white;background: rgba(0, 0, 0, 0.5); "><?=$value['tipo']?></span>
						</div>
					<?php endforeach ?>	
				</div>
			</div>			
		</div>
	</div>
  	<div class="box-footer">
    	<button type="submit" name="btn-submit" value="guardar" class="btn btn-success btn-sm">
    		<i class="fa fa-check"></i> GUARDAR
    	</button>
    	<button type="submit" name="btn-submit" value="cancelar" class="btn btn-danger btn-sm">
    		<i class="fa fa-chevron-left"></i> VOLVER
    	</button>
    	
    	<!--
    	<a href="<?=site_url('previo')?>" class="btn btn-danger btn-sm"><i class="fa fa-chevron-left"></i> VOLVER</a>
    	-->
  	</div>

<?php echo form_close(); ?>

<script type="text/javascript">
jQuery(document).ready(function($) {
	var fileExtension = "";
    $(':file').change(function(){
    	var typeFile  = $('select[name="type"] option:selected').val();
    	var titleFile = ""; //$("#title").val();

        var file = $("#archivos")[0].files[0];
        var fileName = file.name;
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        var fileSize = file.size;
        var fileType = file.type;
        //showMessage("<span class='info'>Archivo para subir: "+fileName+", "+fileSize+" bytes.</span>");
        var formData = new FormData($("#_formGaleria")[0]);
        var message = ""; 
        $.ajax({
            url: '<?=site_url("assets/galeria/image.php")?>',  
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
                $("#imagencargada").empty();
                $("#imagencargada").append('<img src="<?=site_url('assets/img/loading.gif')?>" width="25%"/>');         
            },
            success: function(data){
                var rpta = $.parseJSON(data);
                //message = $("<span class='success'>La imagen ha subido correctamente.</span>");
                if(isImage(fileExtension)){
                    if ( rpta['rpta'] === 'success' ) {
                        //$("#uri_imagen").val(rpta['imagen']);
                        $("#imagen").prop("readonly",true);                        
                    }
                    //$("#divContainerGaleria").empty();
                    $("#divContainerGaleria").append('<div class="float-left" style="position: relative; display: inline-block; text-align: center;"><img src="'+ "<?=site_url()?>" + rpta['uri_path'] + '" width="160" height="90" title="Imagen Representativa del Servicio" class="img-thumbnail"><input type="hidden" name="imagen[]" value="'+rpta['imagen']+'"><input type="hidden" name="tipo[]" value="'+typeFile+'"><input type="hidden" name="titulo[]" value="'+titleFile.trim()+'"><span class="btn btn-danger fa fa-times btn-sm btn-eliminar-imagen" data-uri="'+rpta['imagen']+'" data-option="1" data-id="" style="position: absolute; top: 8px;right: 8px; border: 0.1em solid #fff;" title="Eliminar"></span><span style="position: absolute; bottom: 5px;right: 4.8px; width:93%;text-align: center; color:white;background: rgba(0, 0, 0, 0.5); ">'+typeFile+'</span></div>'); 
                    $("#imagencargada").empty();

                    $('#type').prop('selectedIndex',0);
                    $("#title").val("").focus();
                }
            },
            error: function(){
                //message = $("<span class='error'>Ha ocurrido un error.</span>");
                //showMessage(message);
            }
        });
    });
 
    function isImage(extension){
        switch(extension.toLowerCase()) {
            case 'jpg': case 'gif': case 'png': case 'jpeg':
                return true;
            break;
            default:
                return false;
            break;
        }
    }

    $(document).on('click', '.btn-eliminar-imagen', function(event) {
    	event.preventDefault();
    	var htmlObject = $(this);
    	var id = $(this).data("id");
    	var uri = $(this).data("uri");
    	var option = $(this).data("option");
 		//console.log(id, uri,option);
 		
 		$.confirm({
	        content: function () {
	            var self = this;
	            return $.ajax({
	                url: "<?=site_url('admin/galeria/remove')?>",
	                dataType: 'json',
	                method: 'post',
	                data: {id:id,uri:uri,option:option},
	            }).done(function (data) {
	                if ( data.response === "success" ) {
	                	htmlObject.parent().remove();
	                	self.setTitle('Confirmación');
	                	self.setContent(data.message);	
	                }else{
	                	self.setTitle('Error');
	                	self.setContent(data.message);
	                }
	                
	            }).fail(function(e){
	            	self.setTitle('Error');
	                self.setContent(e.responseText);
	            });
	        },
	        buttons:{
	        	OK:{
	        		text:"Ok",
	        		btnClass:"btn-red",
	        		action:function(event){

	        		}
	        	}
	        },
	         theme: 'material'
	    });
 		//$(this).parent().remove();
    });
});
</script>

<style type="text/css">
._fileUpload{
    position: relative;
    overflow: hidden;
}
._fileUpload input.upload{
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}	
</style>