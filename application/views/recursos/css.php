    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet" media="all" onload="if(media!='all')media='all'">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?=site_url('resources/jquery.confirm/jquery-confirm.min.css')?>">
    
<style type="text/css">
/*font-family: 'Lato', sans-serif;*/
content {
  flex: 1;
}
    body {
        font-family: 'Quicksand'!important;
        display: flex;
  flex-direction: column;
  min-height: 100vh;
    }
    .fa-15x {
        font-size: 15rem
    }
    .h1-lato{
      font-family: 'Lato', sans-serif !important;
      font-weight: 900 !important;
    }
    .h2-lato{
      font-family: 'Lato', sans-serif !important;
      font-weight: 300 !important;
    }
    .bg-light {
      background: #eee !important;
    }
    /*CHAT FC
    .chat-container{
      margin: 0;
      padding: 0;
      width: 100%;
      max-width: 340px;
      height: auto;
      position: fixed;
      bottom: 0px;
      right: 15px;
      z-index: 999;
    }
    .chat-fc-btn{
      width: 100%;
      margin: 0px;
      cursor: pointer;
      user-select: none;
      padding: 5px 0px;
      background: #dc3545;
      text-align:center;
      color: #fff;
    }
    .chat-content{
      margin: 0px;
      padding: 0px;
      background: #fff;
      display: none;
    }
    */
    .menu-custom{
        position: absolute;
        top: 0;
        z-index: 1;
        width: 100%;
        background: #ffffff00 !important;
    }
    .bg-orange{
        background: #FFC107 !important;
    }
    .bg-company{
      background: #dee2e6 !important
    }
    .text-company{
      color: #e96a0d  !important;
    }
    .navbar-main ul li>a{
        color: #607D8B  !important;
        text-transform: uppercase;
    }
    #main-menu .active>a, .navbar-main ul li>a:hover{
        color: #ff5722  !important;
        cursor: pointer;
    }
/**/
    .img-div{
               padding: 0;
    
    overflow: hidden;
        }
        .img-div>img{
            width: 100%;
    height: auto;
    opacity: 1;
    position: absolute;
    left: -100%;
    right: -100%;
    top: -100%;
    bottom: -100%;
    margin: auto;
    min-height: 100%;
    min-width: 100%;
        }
        /*#main-menu .active{
          background: #FFC107 !important;
        }
        #main-menu .active:hover{
          color: #ffffdd !important;
        }*/
        /**/
        .t-0{
          top: 0;
        }
  .border-height-4{
    border-width: 4px !important;
  }
</style>
