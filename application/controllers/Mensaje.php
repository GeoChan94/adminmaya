<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mensaje extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        $this->load->model('Mensaje_model');
        $this->load->model('Previo_model');
    } 
    public function index($id_previo)
	{
		if (!$this->session->userdata("login")) {
            redirect(base_url().'login');
        }
		$data=array();
		$data['mensajes'] = $this->Mensaje_model->get_mensaje_id_previo($id_previo);
		$data['previo'] = $this->Previo_model->get_previo($id_previo);


	$data['_view'] = 'mensaje/index';
		if ($this->session->userdata("tipo_usuario")=='sadmin') {
       		$this->load->view('layouts/admin',$data);
        }else if ($this->session->userdata("tipo_usuario")=='admin') {
       		$this->load->view('layouts/admin',$data);
        }else if ($this->session->userdata("tipo_usuario")=='user') {
       		$this->load->view('layouts/user',$data);
        }

		// echo "hola";
	}
	public function add()
	{
		$data=array();
		$params = array(
			'mensaje'=>$this->input->post('mensaje'),
			'nombre'=>$this->input->post('nombre'),
			'correo'=>$this->input->post('correo'),
			'celular'=>$this->input->post('celular'),
			'previo_id_previo'=>$this->input->post('id_previo'),
            );
            
            $previo_id = $this->Mensaje_model->add_mensaje($params);
            return $previo_id;

		// echo "hola";
	}
	

}
