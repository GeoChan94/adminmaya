<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        $this->load->model('Previo_model');
        $this->load->model('Provincia_previo_model');
        $this->load->model('Modalidad_previo_model');
    } 
	public function index()
	{
        

        $temp_data_previo = $this->Previo_model->get_all_previo();
        $pronvincias_previos=array();
        $modalidad_previo=array();
        $data =array();
        foreach ($temp_data_previo as $key => $value) {
        	$tipo_modalidad=$this->Modalidad_previo_model->get_modalidad_previo($value['modalidad_previo_id_modalidad_previo']);
        	$pronvincias_previos[]=$this->Provincia_previo_model->get_provincia_previo($value['provincia_previo_id_provincia'])['nombre'];
        	// $modalidad_previo[]=array(
        	// 	'id' => $value['modalidad_previo_id_modalidad_previo'], 
        	// 	'nombre' =>$tipo_modalidad['nombre']
        	// );
        	$modalidad_previo[$value['modalidad_previo_id_modalidad_previo']]=$tipo_modalidad['nombre'];

        	switch ($tipo_modalidad['id_modalidad_previo']) {
        		case 1:
	        		if ($value['select_index']==1) {
		        		$data['previo_venta'][]=array( 
							'id_previo'=>$value['id_previo'],
							'titulo'=>$value['titulo'],
							'descripcion_previo'=>$value['descripcion_previo'],
							'ubicacion_geografica'=>$value['ubicacion_geografica'],
							// 'mantenimiento'=>$value['mantenimiento'],
							'area'=>$value['area'],
							'medida_frente'=>$value['medida_frente'],
							'medida_fondo'=>$value['medida_fondo'],
							'uri_imagen_destacada'=>base_url().'assets/galeria/imagenes/'.$value['uri_imagen_destacada'],
							'precio'=>number_format($value['precio'], 2, '.', ','),
							'creacion'=>$value['creacion'],
							'uri_previo'=>base_url().'venta/'.$value['id_previo'].'-'.$value['uri_previo'],
							'tipo_pago_id_tipo_pago'=>$value['tipo_pago_id_tipo_pago'],
							'tipo_previo_id_tipo_previo'=>$value['tipo_previo_id_tipo_previo'],
							'provincia_previo_id_provincia'=>$this->Provincia_previo_model->get_provincia_previo($value['provincia_previo_id_provincia'])['nombre'],
							'modalidad_previo_id_modalidad_previo'=>$tipo_modalidad['nombre'],
							'tipo_documento_previo_id_tipo_documento_previo'=>$value['tipo_documento_previo_id_tipo_documento_previo'],
							'state'=>$value['state'],
			        	);
		        	}
        			
        			break;
        		case 2:
        			if ($value['select_index']==1) {
	        			$data['previo_alquiler'][]=array( 
							'id_previo'=>$value['id_previo'],
							'titulo'=>$value['titulo'],
							'descripcion_previo'=>$value['descripcion_previo'],
							'ubicacion_geografica'=>$value['ubicacion_geografica'],
							// 'mantenimiento'=>$value['mantenimiento'],
							'area'=>$value['area'],
							'medida_frente'=>$value['medida_frente'],
							'medida_fondo'=>$value['medida_fondo'],
							'uri_imagen_destacada'=>base_url().'assets/galeria/imagenes/'.$value['uri_imagen_destacada'],
							'precio'=>number_format($value['precio'], 2, '.', ','),
							'creacion'=>$value['creacion'],
							'uri_previo'=>base_url().'alquiler/'.$value['id_previo'].'-'.$value['uri_previo'],
							'tipo_pago_id_tipo_pago'=>$value['tipo_pago_id_tipo_pago'],
							'tipo_previo_id_tipo_previo'=>$value['tipo_previo_id_tipo_previo'],
							'provincia_previo_id_provincia'=>$this->Provincia_previo_model->get_provincia_previo($value['provincia_previo_id_provincia'])['nombre'],
							'modalidad_previo_id_modalidad_previo'=>$tipo_modalidad['nombre'],
							'tipo_documento_previo_id_tipo_documento_previo'=>$value['tipo_documento_previo_id_tipo_documento_previo'],
							'state'=>$value['state'],
			        	);
	        		}
        			break;
        		case 3:
	        		if ($value['select_index']==1) {
	        			$data['previo_proyecto'][]=array( 
							'id_previo'=>$value['id_previo'],
							'titulo'=>$value['titulo'],
							'descripcion_previo'=>$value['descripcion_previo'],
							'ubicacion_geografica'=>$value['ubicacion_geografica'],
							// 'mantenimiento'=>$value['mantenimiento'],
							'area'=>$value['area'],
							'medida_frente'=>$value['medida_frente'],
							'medida_fondo'=>$value['medida_fondo'],
							'uri_imagen_destacada'=>base_url().'assets/galeria/imagenes/'.$value['uri_imagen_destacada'],
							'precio'=>number_format($value['precio'], 2, '.', ','),
							'creacion'=>$value['creacion'],
							'uri_previo'=>base_url().'proyecto/'.$value['id_previo'].'-'.$value['uri_previo'],
							'tipo_pago_id_tipo_pago'=>$value['tipo_pago_id_tipo_pago'],
							'tipo_previo_id_tipo_previo'=>$value['tipo_previo_id_tipo_previo'],
							'provincia_previo_id_provincia'=>$this->Provincia_previo_model->get_provincia_previo($value['provincia_previo_id_provincia'])['nombre'],
							'modalidad_previo_id_modalidad_previo'=>$tipo_modalidad['nombre'],
							'tipo_documento_previo_id_tipo_documento_previo'=>$value['tipo_documento_previo_id_tipo_documento_previo'],
							'state'=>$value['state'],
			        	);
			        }
        			break;
        			case 4:
        			if ($value['select_index']==1) {
	        			$data['previo_alquiler'][]=array( 
							'id_previo'=>$value['id_previo'],
							'titulo'=>$value['titulo'],
							'descripcion_previo'=>$value['descripcion_previo'],
							'ubicacion_geografica'=>$value['ubicacion_geografica'],
							// 'mantenimiento'=>$value['mantenimiento'],
							'area'=>$value['area'],
							'medida_frente'=>$value['medida_frente'],
							'medida_fondo'=>$value['medida_fondo'],
							'uri_imagen_destacada'=>base_url().'assets/galeria/imagenes/'.$value['uri_imagen_destacada'],
							'precio'=>number_format($value['precio'], 2, '.', ','),
							'creacion'=>$value['creacion'],
							'uri_previo'=>base_url().'anticresis/'.$value['id_previo'].'-'.$value['uri_previo'],
							'tipo_pago_id_tipo_pago'=>$value['tipo_pago_id_tipo_pago'],
							'tipo_previo_id_tipo_previo'=>$value['tipo_previo_id_tipo_previo'],
							'provincia_previo_id_provincia'=>$this->Provincia_previo_model->get_provincia_previo($value['provincia_previo_id_provincia'])['nombre'],
							'modalidad_previo_id_modalidad_previo'=>$tipo_modalidad['nombre'],
							'tipo_documento_previo_id_tipo_documento_previo'=>$value['tipo_documento_previo_id_tipo_documento_previo'],
							'state'=>$value['state'],
			        	);
	        		}
        			break;
        		// default:
        		// 	# code...
        		// 	break;
        	}
        }
        foreach (array_unique($pronvincias_previos) as $key => $value) {
        	$data['pronvincias_previos'][]=array(
        		'id' => $key,
        		'nombre' => $value 
        	);
        }
        foreach ($modalidad_previo as $key => $value) {
        	$data['modalidad_previo'][] = array(
        		'id' => $key,
        		'nombre' => $value 
        	);
        }
        
// var_dump($data);
		$this->load->view('page_index',$data);
		// echo "hola";
	}
	

}
