<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Tipo_pago extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tipo_pago_model');
        if ($this->session->userdata("login")) {
            if ($this->session->userdata("tipo_usuario")=='admin'||$this->session->userdata("tipo_usuario")=='sadmin') {
                
            }else{
                redirect(base_url().'admin');
            }            
        }else {
            redirect(base_url().'login');
        }
    } 

    /*
     * Listing of tipo_pago
     */
    function index()
    {
        $data['tipo_pago'] = $this->Tipo_pago_model->get_all_tipo_pago();
        
        $data['_view'] = 'tipo_pago/index';
        $this->load->view('layouts/admin',$data);
    }

    /*
     * Adding a new tipo_pago
     */
    function add()
    {   
        $this->load->library('form_validation');

		$this->form_validation->set_rules('nombre','Nombre','required|max_length[100]');
		$this->form_validation->set_rules('descripcion','Descripcion','max_length[200]');
		
		if($this->form_validation->run())     
        {   
            $params = array(
				'nombre' => $this->input->post('nombre'),
				'descripcion' => $this->input->post('descripcion'),
            );
            
            $tipo_pago_id = $this->Tipo_pago_model->add_tipo_pago($params);
            redirect('admin/tipo_pago/index');
        }
        else
        {            
            $data['_view'] = 'tipo_pago/add';
            $this->load->view('layouts/admin',$data);
        }
    }  

    /*
     * Editing a tipo_pago
     */
    function edit($id_tipo_pago)
    {   
        // check if the tipo_pago exists before trying to edit it
        $data['tipo_pago'] = $this->Tipo_pago_model->get_tipo_pago($id_tipo_pago);
        
        if(isset($data['tipo_pago']['id_tipo_pago']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('nombre','Nombre','required|max_length[100]');
			$this->form_validation->set_rules('descripcion','Descripcion','max_length[200]');
		
			if($this->form_validation->run())     
            {   
                $params = array(
					'nombre' => $this->input->post('nombre'),
					'descripcion' => $this->input->post('descripcion'),
                );

                $this->Tipo_pago_model->update_tipo_pago($id_tipo_pago,$params);            
                redirect('admin/tipo_pago/index');
            }
            else
            {
                $data['_view'] = 'tipo_pago/edit';
                $this->load->view('layouts/admin',$data);
            }
        }
        else
            show_error('The tipo_pago you are trying to edit does not exist.');
    } 

    /*
     * Deleting tipo_pago
     */
    function remove($id_tipo_pago)
    {
        $tipo_pago = $this->Tipo_pago_model->get_tipo_pago($id_tipo_pago);

        // check if the tipo_pago exists before trying to delete it
        if(isset($tipo_pago['id_tipo_pago']))
        {
            $this->Tipo_pago_model->delete_tipo_pago($id_tipo_pago);
            redirect('admin/tipo_pago/index');
        }
        else
            show_error('The tipo_pago you are trying to delete does not exist.');
    }
    
}
