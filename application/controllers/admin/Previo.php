<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Previo extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Previo_model');
        $this->load->model('Provincia_previo_model');
        $this->load->model('Modalidad_previo_model');
        $this->load->model('Usuario_model');
        $this->load->model('Mensaje_model');
        if (!$this->session->userdata("login")) {
            redirect(base_url().'login');
        }
    } 

    /*
     * Listing of previo
     */
    function index()
    {

        // $params['limit'] = RECORDS_PER_PAGE; 
        // $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        // $config = $this->config->item('pagination');
        // $config['base_url'] = site_url('admin/previo/index?');
        // $config['total_rows'] = $this->Previo_model->get_all_previo_count();
        // $this->pagination->initialize($config);

        // $temp_data_previo = $this->Previo_model->get_all_previo($params);

        

        if ($this->session->userdata("tipo_usuario")=='sadmin') {
        	$temp_data_previo = $this->Previo_model->get_all_previo();
        }else if ($this->session->userdata("tipo_usuario")=='admin') {
        	$temp_data_previo = $this->Previo_model->get_all_previo();
        }else if ($this->session->userdata("tipo_usuario")=='user') {
        	$temp_data_previo = $this->Previo_model->get_all_previo_user($this->session->userdata("id_usuario"));
        }


        
        // var_dump($temp_data_previo);
        // $data['previo'][]=array();
        // $data['previo'][]=array();
        foreach ($temp_data_previo as $key => $value) {
            $usuario=$this->Usuario_model->get_usuario($value['usuario_id_usuario']);
	        		$data['previo'][]=array( 
					'id_previo'=>$value['id_previo'],
					'titulo'=>$value['titulo'],
					'descripcion_previo'=>$value['descripcion_previo'],
					'ubicacion_geografica'=>$value['ubicacion_geografica'],
					// 'mantenimiento'=>$value['mantenimiento'],
					'area'=>$value['area'],
					'medida_frente'=>$value['medida_frente'],
					'medida_fondo'=>$value['medida_fondo'],
					'uri_imagen_destacada'=>$value['uri_imagen_destacada'],
					'precio'=>$value['precio'],
					'creacion'=>$value['creacion'],
					'uri_previo'=>$value['uri_previo'],
					'tipo_pago_id_tipo_pago'=>$value['tipo_pago_id_tipo_pago'],
					'tipo_previo_id_tipo_previo'=>$value['tipo_previo_id_tipo_previo'],
					'provincia_previo_id_provincia'=>$this->Provincia_previo_model->get_provincia_previo($value['provincia_previo_id_provincia'])['nombre'],
					'modalidad_previo_id_modalidad_previo'=>$this->Modalidad_previo_model->get_modalidad_previo($value['modalidad_previo_id_modalidad_previo'])['nombre'],
					'tipo_documento_previo_id_tipo_documento_previo'=>$value['tipo_documento_previo_id_tipo_documento_previo'],
					'state'=>$value['state'],
                    'mensaje'=>count($this->Mensaje_model->get_mensaje_id_previo($value['id_previo'])),
                    'id_mensaje'=>@$this->Mensaje_model->get_mensaje_id_previo($value['id_previo'])['id_mensaje'],
                    'nombres'=>@$usuario['nombres'],
                    'usuario'=>@$usuario['usuario'],


	        	);
        	
        }
// var_dump($data['previo']);
        if ($this->session->userdata("tipo_usuario")=='sadmin') {
        	$data['_view'] = 'previo/index';
       		$this->load->view('layouts/admin',$data);
        }else if ($this->session->userdata("tipo_usuario")=='admin') {
        	$data['_view'] = 'previo/index';
       		$this->load->view('layouts/admin',$data);
        }else if ($this->session->userdata("tipo_usuario")=='user') {
        	$data['_view'] = 'previo/index';
       		$this->load->view('layouts/user',$data);
        }
        
    }

    /*
     * Adding a new previo
     */
    function add()
    {   
        $this->load->library('form_validation');

		$this->form_validation->set_rules('titulo','Titulo','required|max_length[200]');
		$this->form_validation->set_rules('ubicacion_geografica','Ubicacion Geografica','required|max_length[50]');
		// $this->form_validation->set_rules('mantenimiento','Mantenimiento','numeric');
		$this->form_validation->set_rules('area','Area','required');
		$this->form_validation->set_rules('medida_frente','Medida Frente','required');
		$this->form_validation->set_rules('medida_fondo','Medida Fondo','required');
		//$this->form_validation->set_rules('uri_imagen_destacada','Uri Imagen Destacada','required|max_length[100]');
		$this->form_validation->set_rules('uri_imagen','Uri Imagen Destacada','required|max_length[100]');
		$this->form_validation->set_rules('precio','Precio','required');
		// $this->form_validation->set_rules('creacion','Creacion','required');
		$this->form_validation->set_rules('uri_previo','Uri Previo','required|max_length[150]');
		$this->form_validation->set_rules('tipo_pago_id_tipo_pago','Tipo Pago Id Tipo Pago','required|integer');
		$this->form_validation->set_rules('tipo_previo_id_tipo_previo','Tipo Previo Id Tipo Previo','required|integer');
		$this->form_validation->set_rules('provincia_previo_id_provincia','Provincia Previo Id Provincia','required|integer');
		$this->form_validation->set_rules('modalidad_previo_id_modalidad_previo','Modalidad Previo Id Modalidad Previo','required|integer');
		$this->form_validation->set_rules('tipo_documento_previo_id_tipo_documento_previo','Tipo Documento Previo Id Tipo Documento Previo','required|integer');
		$this->form_validation->set_rules('descripcion_previo','Descripcion Previo','required');
		
		if($this->form_validation->run())     
        {   
            $params = array(
				'tipo_pago_id_tipo_pago' => $this->input->post('tipo_pago_id_tipo_pago'),
				'tipo_previo_id_tipo_previo' => $this->input->post('tipo_previo_id_tipo_previo'),
				'provincia_previo_id_provincia' => $this->input->post('provincia_previo_id_provincia'),
				'modalidad_previo_id_modalidad_previo' => $this->input->post('modalidad_previo_id_modalidad_previo'),
				'tipo_documento_previo_id_tipo_documento_previo' => $this->input->post('tipo_documento_previo_id_tipo_documento_previo'),
				'titulo' => $this->input->post('titulo'),
				'ubicacion_geografica' => $this->input->post('ubicacion_geografica'),
				// 'mantenimiento' => $this->input->post('mantenimiento'),
				'area' => $this->input->post('area'),
				'medida_frente' => $this->input->post('medida_frente'),
				'medida_fondo' => $this->input->post('medida_fondo'),
				//'uri_imagen_destacada' => $this->input->post('uri_imagen_destacada'),
				'uri_imagen_destacada' => $this->input->post('uri_imagen'),
                'uri_video' => @$this->input->post('uri_video'),
				'precio' => $this->input->post('precio'),
				// 'creacion' => $this->input->post('creacion'),
				'uri_previo' => $this->input->post('uri_previo'),
				'descripcion_previo' => $this->input->post('descripcion_previo'),
				'usuario_id_usuario'=>$this->session->userdata("id_usuario"),
            );
            
            $previo_id = $this->Previo_model->add_previo($params);
            redirect('admin/previo/index');
        }
        else
        {
			$this->load->model('Tipo_pago_model');
			$data['all_tipo_pago'] = $this->Tipo_pago_model->get_all_tipo_pago();

			$this->load->model('Tipo_previo_model');
			$data['all_tipo_previo'] = $this->Tipo_previo_model->get_all_tipo_previo();

			$this->load->model('Provincia_previo_model');
			$data['all_provincia_previo'] = $this->Provincia_previo_model->get_all_provincia_previo();

			$this->load->model('Modalidad_previo_model');
			$data['all_modalidad_previo'] = $this->Modalidad_previo_model->get_all_modalidad_previo();

			$this->load->model('Tipo_documento_previo_model');
			$data['all_tipo_documento_previo'] = $this->Tipo_documento_previo_model->get_all_tipo_documento_previo();
            
            $data['_view'] = 'previo/add';
            	if ($this->session->userdata("login")) {
                    if ($this->session->userdata("tipo_usuario")=='sadmin') {
                        $this->load->view('layouts/sadmin',$data);
                    }else if ($this->session->userdata("tipo_usuario")=='admin') {
                        $this->load->view('layouts/admin',$data);
                    }else{
                        $this->load->view('layouts/user',$data);
                    }            
                }else {
                    redirect(base_url().'login');
                }
        }
    }  

    /*
     * Editing a previo
     */
    function edit($id_previo)
    {   
        // check if the previo exists before trying to edit it
        $data['previo'] = $this->Previo_model->get_previo($id_previo);

        if(isset($data['previo']['id_previo'])){
            $this->load->library('form_validation');

			$this->form_validation->set_rules('titulo','Titulo','required|max_length[200]');
			$this->form_validation->set_rules('ubicacion_geografica','Ubicacion Geografica','required|max_length[50]');
			// $this->form_validation->set_rules('mantenimiento','Mantenimiento','numeric');
			$this->form_validation->set_rules('area','Area','required');
			$this->form_validation->set_rules('medida_frente','Medida Frente','required');
			$this->form_validation->set_rules('medida_fondo','Medida Fondo','required');
			//$this->form_validation->set_rules('uri_imagen_destacada','Uri Imagen Destacada','required|max_length[100]');
			$this->form_validation->set_rules('uri_imagen','Uri Imagen Destacada','required|max_length[100]');
			$this->form_validation->set_rules('precio','Precio','required');
			// $this->form_validation->set_rules('creacion','Creacion','required');
			$this->form_validation->set_rules('uri_previo','Uri Previo','required|max_length[150]');
			$this->form_validation->set_rules('tipo_pago_id_tipo_pago','Tipo Pago Id Tipo Pago','required|integer');
			$this->form_validation->set_rules('tipo_previo_id_tipo_previo','Tipo Previo Id Tipo Previo','required|integer');
			$this->form_validation->set_rules('provincia_previo_id_provincia','Provincia Previo Id Provincia','required|integer');
			$this->form_validation->set_rules('modalidad_previo_id_modalidad_previo','Modalidad Previo Id Modalidad Previo','required|integer');
			$this->form_validation->set_rules('tipo_documento_previo_id_tipo_documento_previo','Tipo Documento Previo Id Tipo Documento Previo','required|integer');
			$this->form_validation->set_rules('descripcion_previo','Descripcion Previo','required');
		
			if($this->form_validation->run()){   
                $params = array(
					'tipo_pago_id_tipo_pago' => $this->input->post('tipo_pago_id_tipo_pago'),
					'tipo_previo_id_tipo_previo' => $this->input->post('tipo_previo_id_tipo_previo'),
					'provincia_previo_id_provincia' => $this->input->post('provincia_previo_id_provincia'),
					'modalidad_previo_id_modalidad_previo' => $this->input->post('modalidad_previo_id_modalidad_previo'),
					'tipo_documento_previo_id_tipo_documento_previo' => $this->input->post('tipo_documento_previo_id_tipo_documento_previo'),
					'titulo' => $this->input->post('titulo'),
					'ubicacion_geografica' => $this->input->post('ubicacion_geografica'),
					// 'mantenimiento' => $this->input->post('mantenimiento'),
					'area' => $this->input->post('area'),
					'medida_frente' => $this->input->post('medida_frente'),
					'medida_fondo' => $this->input->post('medida_fondo'),
					//'uri_imagen_destacada' => $this->input->post('uri_imagen_destacada'),
					'uri_imagen_destacada' => $this->input->post('uri_imagen'),
                    'uri_video' => @$this->input->post('uri_video'),
					'precio' => $this->input->post('precio'),
					// 'creacion' => $this->input->post('creacion'),
					'uri_previo' => $this->input->post('uri_previo'),
					'descripcion_previo' => $this->input->post('descripcion_previo'),
                );

                $this->Previo_model->update_previo($id_previo,$params);            
                redirect('admin/previo/index');
            }else{
				$this->load->model('Tipo_pago_model');
				$data['all_tipo_pago'] = $this->Tipo_pago_model->get_all_tipo_pago();

				$this->load->model('Tipo_previo_model');
				$data['all_tipo_previo'] = $this->Tipo_previo_model->get_all_tipo_previo();

				$this->load->model('Provincia_previo_model');
				$data['all_provincia_previo'] = $this->Provincia_previo_model->get_all_provincia_previo();

				$this->load->model('Modalidad_previo_model');
				$data['all_modalidad_previo'] = $this->Modalidad_previo_model->get_all_modalidad_previo();

				$this->load->model('Tipo_documento_previo_model');
				$data['all_tipo_documento_previo'] = $this->Tipo_documento_previo_model->get_all_tipo_documento_previo();

                $data['_view'] = 'previo/edit';
                if ($this->session->userdata("login")) {
                    if ($this->session->userdata("tipo_usuario")=='sadmin') {
                        $this->load->view('layouts/sadmin',$data);
                    }else if ($this->session->userdata("tipo_usuario")=='admin') {
                        $this->load->view('layouts/admin',$data);
                    }else{
                        $this->load->view('layouts/user',$data);
                    }            
                }else {
                    redirect(base_url().'login');
                }
            }
        }else{
            show_error('The previo you are trying to edit does not exist.');
        }
    } 

    /*
     * Deleting previo
     */
    function remove($id_previo)
    {
        $previo = $this->Previo_model->get_previo($id_previo);

        // check if the previo exists before trying to delete it
        if(isset($previo['id_previo']))
        {
            $this->Previo_model->delete_previo($id_previo);
            redirect('admin/previo/index');
        }
        else
            show_error('The previo you are trying to delete does not exist.');
    }
    function edit_check_index($id_previo){
                $params = array(
					'select_index' => $this->input->post('select_index'),
                );

                $this->Previo_model->update_previo($id_previo,$params);            
                redirect('admin/propiedades_seleccionadas/index');

    }
    function vender($id_previo){
			$params = array(
					'state' => '1',
                );
                $this->Previo_model->update_previo($id_previo,$params);            
                redirect('admin/previo/index');

    }
    function desvender($id_previo){
			$params = array(
					'state' => '0',
                );
                $this->Previo_model->update_previo($id_previo,$params);            
                redirect('admin/previo/index');

    }
    
    
}
