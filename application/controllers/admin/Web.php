<?php

class Web extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Previo_model');
        if (!$this->session->userdata("login")) {
            redirect(base_url().'login');
        }
        
    } 

    function index(){
 		$data['points'] = $this->Previo_model->get_all_points();
 		$this->load->view('web/index',$data);
    }


}