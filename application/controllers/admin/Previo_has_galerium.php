<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Previo_has_galerium extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Previo_has_galerium_model');
        if (!$this->session->userdata("login")) {
            redirect(base_url().'login');
        }
    } 

    /*
     * Listing of previo_has_galeria
     */
    function index()
    {
        $data['previo_has_galeria'] = $this->Previo_has_galerium_model->get_all_previo_has_galeria();
        
        $data['_view'] = 'previo_has_galerium/index';
        $this->load->view('layouts/admin',$data);
    }

    /*
     * Adding a new previo_has_galerium
     */
    function add()
    {   
        $this->load->library('form_validation');

		$this->form_validation->set_rules('previo_id_previo','Previo Id Previo','required|integer');
		$this->form_validation->set_rules('galeria_id_galeria','Galeria Id Galeria','required|integer');
		
		if($this->form_validation->run())     
        {   
            $params = array(
				'previo_id_previo' => $this->input->post('previo_id_previo'),
				'galeria_id_galeria' => $this->input->post('galeria_id_galeria'),
            );
            
            $previo_has_galerium_id = $this->Previo_has_galerium_model->add_previo_has_galerium($params);
            redirect('admin/previo_has_galerium/index');
        }
        else
        {
			$this->load->model('Previo_model');
			$data['all_previo'] = $this->Previo_model->get_all_previo();

			$this->load->model('Galerium_model');
			$data['all_galeria'] = $this->Galerium_model->get_all_galeria();
            
            $data['_view'] = 'previo_has_galerium/add';
            $this->load->view('layouts/admin',$data);
        }
    }  

    /*
     * Editing a previo_has_galerium
     */
    function edit($id_previo_has_galeriacol)
    {   
        // check if the previo_has_galerium exists before trying to edit it
        $data['previo_has_galerium'] = $this->Previo_has_galerium_model->get_previo_has_galerium($id_previo_has_galeriacol);
        
        if(isset($data['previo_has_galerium']['id_previo_has_galeriacol']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('previo_id_previo','Previo Id Previo','required|integer');
			$this->form_validation->set_rules('galeria_id_galeria','Galeria Id Galeria','required|integer');
		
			if($this->form_validation->run())     
            {   
                $params = array(
					'previo_id_previo' => $this->input->post('previo_id_previo'),
					'galeria_id_galeria' => $this->input->post('galeria_id_galeria'),
                );

                $this->Previo_has_galerium_model->update_previo_has_galerium($id_previo_has_galeriacol,$params);            
                redirect('admin/previo_has_galerium/index');
            }
            else
            {
				$this->load->model('Previo_model');
				$data['all_previo'] = $this->Previo_model->get_all_previo();

				$this->load->model('Galerium_model');
				$data['all_galeria'] = $this->Galerium_model->get_all_galeria();

                $data['_view'] = 'previo_has_galerium/edit';
                $this->load->view('layouts/admin',$data);
            }
        }
        else
            show_error('The previo_has_galerium you are trying to edit does not exist.');
    } 

    /*
     * Deleting previo_has_galerium
     */
    function remove($id_previo_has_galeriacol)
    {
        $previo_has_galerium = $this->Previo_has_galerium_model->get_previo_has_galerium($id_previo_has_galeriacol);

        // check if the previo_has_galerium exists before trying to delete it
        if(isset($previo_has_galerium['id_previo_has_galeriacol']))
        {
            $this->Previo_has_galerium_model->delete_previo_has_galerium($id_previo_has_galeriacol);
            redirect('admin/previo_has_galerium/index');
        }
        else
            show_error('The previo_has_galerium you are trying to delete does not exist.');
    }
    
}
