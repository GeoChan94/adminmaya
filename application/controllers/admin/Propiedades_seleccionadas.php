<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class propiedades_seleccionadas extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Previo_model');
        $this->load->model('Provincia_previo_model');
        $this->load->model('Modalidad_previo_model');
        if ($this->session->userdata("login")) {
            if ($this->session->userdata("tipo_usuario")=='admin'||$this->session->userdata("tipo_usuario")=='sadmin') {
                
            }else{
                redirect(base_url().'admin');
            }            
        }else {
            redirect(base_url().'login');
        }
    } 

    /*
     * Listing of caracteristica
     */
    function index()
    {
    

        $temp_data_previo = $this->Previo_model->get_all_previo();
        $pronvincias_previos=array();
        $modalidad_previo=array();
        // var_dump($temp_data_previo);
        foreach ($temp_data_previo as $key => $value) {
            $tipo_modalidad=$this->Modalidad_previo_model->get_modalidad_previo($value['modalidad_previo_id_modalidad_previo']);
            $pronvincias_previos[]=$this->Provincia_previo_model->get_provincia_previo($value['provincia_previo_id_provincia'])['nombre'];
            // $modalidad_previo[]=array(
            //  'id' => $value['modalidad_previo_id_modalidad_previo'], 
            //  'nombre' =>$tipo_modalidad['nombre']
            // );
            $modalidad_previo[$value['modalidad_previo_id_modalidad_previo']]=$tipo_modalidad['nombre'];
            switch ($tipo_modalidad['id_modalidad_previo']) {
                case 1:
                    $data['previo_venta'][]=array( 
                        'id_previo'=>$value['id_previo'],
                        'titulo'=>$value['titulo'],
                        'descripcion_previo'=>$value['descripcion_previo'],
                        'ubicacion_geografica'=>$value['ubicacion_geografica'],
                        // 'mantenimiento'=>$value['mantenimiento'],
                        'area'=>$value['area'],
                        'medida_frente'=>$value['medida_frente'],
                        'medida_fondo'=>$value['medida_fondo'],
                        'uri_imagen_destacada'=>base_url().'assets/galeria/imagenes/'.$value['uri_imagen_destacada'],
                        'precio'=>number_format($value['precio'], 2, '.', ','),
                        'creacion'=>$value['creacion'],
                        'uri_previo'=>base_url().'venta/'.$value['id_previo'].'-'.$value['uri_previo'],
                        'tipo_pago_id_tipo_pago'=>$value['tipo_pago_id_tipo_pago'],
                        'tipo_previo_id_tipo_previo'=>$value['tipo_previo_id_tipo_previo'],
                        'provincia_previo_id_provincia'=>$this->Provincia_previo_model->get_provincia_previo($value['provincia_previo_id_provincia'])['nombre'],
                        'modalidad_previo_id_modalidad_previo'=>$tipo_modalidad['nombre'],
                        'tipo_documento_previo_id_tipo_documento_previo'=>$value['tipo_documento_previo_id_tipo_documento_previo'],
                        'select_index'=>$value['select_index'],
                        'state'=>$value['state'],
                    );
                    break;
                case 2:
                    $data['previo_alquiler'][]=array( 
                        'id_previo'=>$value['id_previo'],
                        'titulo'=>$value['titulo'],
                        'descripcion_previo'=>$value['descripcion_previo'],
                        'ubicacion_geografica'=>$value['ubicacion_geografica'],
                        // 'mantenimiento'=>$value['mantenimiento'],
                        'area'=>$value['area'],
                        'medida_frente'=>$value['medida_frente'],
                        'medida_fondo'=>$value['medida_fondo'],
                        'uri_imagen_destacada'=>base_url().'assets/galeria/imagenes/'.$value['uri_imagen_destacada'],
                        'precio'=>number_format($value['precio'], 2, '.', ','),
                        'creacion'=>$value['creacion'],
                        'uri_previo'=>base_url().'alquiler/'.$value['id_previo'].'-'.$value['uri_previo'],
                        'tipo_pago_id_tipo_pago'=>$value['tipo_pago_id_tipo_pago'],
                        'tipo_previo_id_tipo_previo'=>$value['tipo_previo_id_tipo_previo'],
                        'provincia_previo_id_provincia'=>$this->Provincia_previo_model->get_provincia_previo($value['provincia_previo_id_provincia'])['nombre'],
                        'modalidad_previo_id_modalidad_previo'=>$tipo_modalidad['nombre'],
                        'tipo_documento_previo_id_tipo_documento_previo'=>$value['tipo_documento_previo_id_tipo_documento_previo'],
                        'select_index'=>$value['select_index'],
                        'state'=>$value['state'],
                    );
                    break;
                case 3:
                    $data['previo_proyecto'][]=array( 
                        'id_previo'=>$value['id_previo'],
                        'titulo'=>$value['titulo'],
                        'descripcion_previo'=>$value['descripcion_previo'],
                        'ubicacion_geografica'=>$value['ubicacion_geografica'],
                        // 'mantenimiento'=>$value['mantenimiento'],
                        'area'=>$value['area'],
                        'medida_frente'=>$value['medida_frente'],
                        'medida_fondo'=>$value['medida_fondo'],
                        'uri_imagen_destacada'=>base_url().'assets/galeria/imagenes/'.$value['uri_imagen_destacada'],
                        'precio'=>number_format($value['precio'], 2, '.', ','),
                        'creacion'=>$value['creacion'],
                        'uri_previo'=>base_url().'proyecto/'.$value['id_previo'].'-'.$value['uri_previo'],
                        'tipo_pago_id_tipo_pago'=>$value['tipo_pago_id_tipo_pago'],
                        'tipo_previo_id_tipo_previo'=>$value['tipo_previo_id_tipo_previo'],
                        'provincia_previo_id_provincia'=>$this->Provincia_previo_model->get_provincia_previo($value['provincia_previo_id_provincia'])['nombre'],
                        'modalidad_previo_id_modalidad_previo'=>$tipo_modalidad['nombre'],
                        'tipo_documento_previo_id_tipo_documento_previo'=>$value['tipo_documento_previo_id_tipo_documento_previo'],
                        'select_index'=>$value['select_index'],
                        'state'=>$value['state'],
                    );
                    break;
                    case 4:
                    $data['previo_alquiler'][]=array( 
                        'id_previo'=>$value['id_previo'],
                        'titulo'=>$value['titulo'],
                        'descripcion_previo'=>$value['descripcion_previo'],
                        'ubicacion_geografica'=>$value['ubicacion_geografica'],
                        // 'mantenimiento'=>$value['mantenimiento'],
                        'area'=>$value['area'],
                        'medida_frente'=>$value['medida_frente'],
                        'medida_fondo'=>$value['medida_fondo'],
                        'uri_imagen_destacada'=>base_url().'assets/galeria/imagenes/'.$value['uri_imagen_destacada'],
                        'precio'=>number_format($value['precio'], 2, '.', ','),
                        'creacion'=>$value['creacion'],
                        'uri_previo'=>base_url().'anticresis/'.$value['id_previo'].'-'.$value['uri_previo'],
                        'tipo_pago_id_tipo_pago'=>$value['tipo_pago_id_tipo_pago'],
                        'tipo_previo_id_tipo_previo'=>$value['tipo_previo_id_tipo_previo'],
                        'provincia_previo_id_provincia'=>$this->Provincia_previo_model->get_provincia_previo($value['provincia_previo_id_provincia'])['nombre'],
                        'modalidad_previo_id_modalidad_previo'=>$tipo_modalidad['nombre'],
                        'tipo_documento_previo_id_tipo_documento_previo'=>$value['tipo_documento_previo_id_tipo_documento_previo'],
                        'select_index'=>$value['select_index'],
                        'state'=>$value['state'],
                    );
                    break;
                // default:
                //  # code...
                //  break;
            }
        }
        foreach (array_unique($pronvincias_previos) as $key => $value) {
            $data['pronvincias_previos'][]=$value;
        }
        foreach ($modalidad_previo as $key => $value) {
            $data['modalidad_previo'][] = array(
                'id' => $key,
                'nombre' => $value 
            );
        }

        $data['_view'] = 'propiedades_seleccionadas/index';

        $this->load->view('layouts/admin',$data);
    }


    
}