<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Propiedades extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        $this->load->model('Previo_model');
        $this->load->model('Provincia_previo_model');
        $this->load->model('Modalidad_previo_model');
        $this->load->model('Tipo_pago_model');
        $this->load->model('Tipo_previo_model');
        $this->load->model('Tipo_documento_previo_model');
        $this->load->model('Galeria_previo_model');
        $this->load->model('Previo_has_servicio_model');
        $this->load->model('Previo_has_caracteristica_model');
        
        

    } 
	public function index()
	{
		$this->load->view('page_alquiler');
		// echo "hola";
	}
	public function previo($id_previo){
		$array_uri = explode("-", $id_previo);
		// var_dump($array_uri);
		$galeria=array();
		$galeria_array=$this->Galeria_previo_model->get_galeria($array_uri[0]);
		foreach ($galeria_array as $key => $value) {
			$galeria[]=base_url().'/assets/galeria/imagenes/'.$value['uri_galeria'];
		}
		// $servicios=array();
		$data['servicios']=$this->Previo_has_servicio_model->get_previo_has_servicio_idprevio($array_uri[0]);
		$data['caracteristicas']=$this->Previo_has_caracteristica_model->get_previo_has_caracteristica_idprevio($array_uri[0]);
		
		// var_dump($galeria);
		$data['galeria']=$galeria;
		$previo=$this->Previo_model->get_previo($array_uri[0]);
		$data['galeria'][]=base_url().'assets/galeria/imagenes/'.$previo['uri_imagen_destacada'];
		
		$data['previo']= array(
			'id_previo'=>$previo['id_previo'],
			'titulo'=>$previo['titulo'],
			'descripcion_previo'=>$previo['descripcion_previo'],
			'ubicacion_geografica'=>$previo['ubicacion_geografica'],
			// 'mantenimiento'=>$previo['mantenimiento'],
			'area'=>number_format($previo['area'], 1, '.', ','),
			'medida_frente'=>number_format($previo['medida_frente'], 1, '.', ','),
			'medida_fondo'=>number_format($previo['medida_fondo'], 1, '.', ','),
			'uri_imagen_destacada'=>$previo['uri_imagen_destacada'],
			'precio'=>number_format($previo['precio'], 2, '.', ','),
			'creacion'=>$previo['creacion'],
			'uri_previo'=>$previo['uri_previo'],
			'uri_video'=>$previo['uri_video'],
			'tipo_pago_id_tipo_pago'=>$this->Tipo_pago_model->get_tipo_pago($previo['tipo_pago_id_tipo_pago'])['nombre'],
			'tipo_previo_id_tipo_previo'=>$this->Tipo_previo_model->get_tipo_previo($previo['tipo_previo_id_tipo_previo'])['nombre'],
			'provincia_previo_id_provincia'=>$this->Provincia_previo_model->get_provincia_previo($previo['provincia_previo_id_provincia'])['nombre'],
			'modalidad_previo_id_modalidad_previo'=>$this->Modalidad_previo_model->get_modalidad_previo($previo['modalidad_previo_id_modalidad_previo'])['nombre'],
			'tipo_documento_previo_id_tipo_documento_previo'=>$this->Tipo_documento_previo_model->get_tipo_documento_previo($previo['tipo_documento_previo_id_tipo_documento_previo'])['nombre'],
		);

		$this->load->view('page_previo_details',$data);


	}
	// public function venta($id_previo){
	// 	$array_uri = explode("-", $id_previo);
	// 	// var_dump($array_uri);
	// 	$proyecto=$this->Previo_model->get_previo($array_uri[0]);
		
	// 	$data['previo']= array(
	// 		'id_previo'=>$proyecto['id_previo'],
	// 		'titulo'=>$proyecto['titulo'],
	// 		'descripcion_previo'=>$proyecto['descripcion_previo'],
	// 		'ubicacion_geografica'=>$proyecto['ubicacion_geografica'],
	// 		// 'mantenimiento'=>$proyecto['mantenimiento'],
	// 		'area'=>number_format($proyecto['area'], 1, '.', ','),
	// 		'medida_frente'=>number_format($proyecto['medida_frente'], 1, '.', ','),
	// 		'medida_fondo'=>number_format($proyecto['medida_fondo'], 1, '.', ','),
	// 		'uri_imagen_destacada'=>$proyecto['uri_imagen_destacada'],
	// 		'precio'=>number_format($proyecto['precio'], 2, '.', ','),
	// 		'creacion'=>$proyecto['creacion'],
	// 		'uri_previo'=>$proyecto['uri_previo'],
	// 		'tipo_pago_id_tipo_pago'=>$this->Tipo_pago_model->get_tipo_pago($proyecto['tipo_pago_id_tipo_pago'])['nombre'],
	// 		'tipo_previo_id_tipo_previo'=>$this->Tipo_previo_model->get_tipo_previo($proyecto['tipo_previo_id_tipo_previo'])['nombre'],
	// 		'provincia_previo_id_provincia'=>$this->Provincia_previo_model->get_provincia_previo($proyecto['provincia_previo_id_provincia'])['nombre'],
	// 		'modalidad_previo_id_modalidad_previo'=>$this->Modalidad_previo_model->get_modalidad_previo($proyecto['modalidad_previo_id_modalidad_previo'])['nombre'],
	// 		'tipo_documento_previo_id_tipo_documento_previo'=>$this->Tipo_documento_previo_model->get_tipo_documento_previo($proyecto['tipo_documento_previo_id_tipo_documento_previo'])['nombre'],
	// 	);
	// 	$this->load->view('page_venta_details',$data);

	// }
	// public function alquiler($id_previo){
	// 	$array_uri = explode("-", $id_previo);
	// 	// var_dump($array_uri);
	// 	$proyecto=$this->Previo_model->get_previo($array_uri[0]);
		
	// 	$data['proyecto']= array(
	// 		'id_previo'=>$proyecto['id_previo'],
	// 		'titulo'=>$proyecto['titulo'],
	// 		'descripcion_previo'=>$proyecto['descripcion_previo'],
	// 		'ubicacion_geografica'=>$proyecto['ubicacion_geografica'],
	// 		// 'mantenimiento'=>$proyecto['mantenimiento'],
	// 		'area'=>number_format($proyecto['area'], 1, '.', ','),
	// 		'medida_frente'=>number_format($proyecto['medida_frente'], 1, '.', ','),
	// 		'medida_fondo'=>number_format($proyecto['medida_fondo'], 1, '.', ','),
	// 		'uri_imagen_destacada'=>$proyecto['uri_imagen_destacada'],
	// 		'precio'=>number_format($proyecto['precio'], 2, '.', ','),
	// 		'creacion'=>$proyecto['creacion'],
	// 		'uri_previo'=>$proyecto['uri_previo'],
	// 		'tipo_pago_id_tipo_pago'=>$this->Tipo_pago_model->get_tipo_pago($proyecto['tipo_pago_id_tipo_pago'])['nombre'],
	// 		'tipo_previo_id_tipo_previo'=>$this->Tipo_previo_model->get_tipo_previo($proyecto['tipo_previo_id_tipo_previo'])['nombre'],
	// 		'provincia_previo_id_provincia'=>$this->Provincia_previo_model->get_provincia_previo($proyecto['provincia_previo_id_provincia'])['nombre'],
	// 		'modalidad_previo_id_modalidad_previo'=>$this->Modalidad_previo_model->get_modalidad_previo($proyecto['modalidad_previo_id_modalidad_previo'])['nombre'],
	// 		'tipo_documento_previo_id_tipo_documento_previo'=>$this->Tipo_documento_previo_model->get_tipo_documento_previo($proyecto['tipo_documento_previo_id_tipo_documento_previo'])['nombre'],
	// 	);
	// 	$this->load->view('page_proyecto_details',$data);

	// }
	public function buscar($modalidad,$provincia){
		$modalidad=str_replace("-"," ",$modalidad);
		$provincia=str_replace("-"," ",$provincia);
		$data=array();
		$modaliad_array=$this->Modalidad_previo_model->get_modalidad_previo_nombre($modalidad);
		$data['modalidad']=$modaliad_array;
		$provincia_array=$this->Provincia_previo_model->get_provincia_previo_nombre($provincia);
		$data['provincia']=$provincia_array;
		$previo_array=$this->Previo_model->get_previo_modalidad($modaliad_array['id_modalidad_previo'],$provincia_array['id_provincia']);
		$previo=array();
		// $data['modaliad']=$modaliad_array;
		
		$now = new DateTime();


		foreach ($previo_array as $key => $value) {
			$time = strtotime($value['creacion']);
			$previo[]= array(
				'id_previo'=>$value['id_previo'],
				'titulo'=>$value['titulo'],
				'descripcion_previo'=>$value['descripcion_previo'],
				'ubicacion_geografica'=>$value['ubicacion_geografica'],
				// 'mantenimiento'=>$value['mantenimiento'],
				'area'=>number_format($value['area'], 1, '.', ','),
				'medida_frente'=>number_format($value['medida_frente'], 1, '.', ','),
				'medida_fondo'=>number_format($value['medida_fondo'], 1, '.', ','),
				'uri_imagen_destacada'=>base_url().'/assets/galeria/imagenes/'.$value['uri_imagen_destacada'],
				'precio'=>number_format($value['precio'], 2, '.', ','),
				'creacion'=>date('d-m-Y',$time),
				'uri_previo'=>base_url().str_replace(" ","-",$modaliad_array['nombre']).'/'.$value['id_previo'].'-'.$value['uri_previo'],
				'tipo_pago_id_tipo_pago'=>$this->Tipo_pago_model->get_tipo_pago($value['tipo_pago_id_tipo_pago'])['nombre'],
				'tipo_previo_id_tipo_previo'=>$this->Tipo_previo_model->get_tipo_previo($value['tipo_previo_id_tipo_previo'])['nombre'],
				'provincia_previo_id_provincia'=>$this->Provincia_previo_model->get_provincia_previo($value['provincia_previo_id_provincia'])['nombre'],
				'modalidad_previo_id_modalidad_previo'=>$this->Modalidad_previo_model->get_modalidad_previo($value['modalidad_previo_id_modalidad_previo'])['nombre'],
				'tipo_documento_previo_id_tipo_documento_previo'=>$this->Tipo_documento_previo_model->get_tipo_documento_previo($value['tipo_documento_previo_id_tipo_documento_previo'])['nombre'],
			);
		}
		$data['previo']=$previo;
		// var_dump($data['previo']);

		
		$this->load->view('page_buscar_previo',$data);

	}
	function mensaje_add(){
		
	}
	

}
