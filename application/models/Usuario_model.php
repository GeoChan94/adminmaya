<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Usuario_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get usuario by id_usuario
     */
    function get_usuario($id_usuario)
    {
        return $this->db->get_where('usuario',array('id_usuario'=>$id_usuario))->row_array();
    }
        
    /*
     * Get all usuario
     */
    function get_all_usuario()
    {
        $this->db->order_by('id_usuario', 'desc');
        return $this->db->get('usuario')->result_array();
    }
        
    /*
     * function to add new usuario
     */
    function add_usuario($params)
    {
        $this->db->insert('usuario',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update usuario
     */
    function update_usuario($id_usuario,$params)
    {
        $this->db->where('id_usuario',$id_usuario);
        return $this->db->update('usuario',$params);
    }
    
    /*
     * function to delete usuario
     */
    function delete_usuario($id_usuario)
    {
        return $this->db->delete('usuario',array('id_usuario'=>$id_usuario));
    }
    public function login($username,$password){
        $this->db->where("usuario",$username);
        $this->db->where("contrasenia",$password);

        $result=$this->db->get("usuario");
        if ($result->num_rows()>0) {
            return $result->row();
        }else{
            return false;
        }
    }

    function get_login( $user = null, $password = null ){
        $data = null;
        $loginResponse = null;
        
            $loginResponse = $this->db->query("SELECT usua.id_usuario,usua.usuario,tipo.id_tipo_usuario,tipo.nombre FROM usuario AS usua JOIN tipo_usuario AS tipo ON usua.tipo_usuario_id_tipo_usuario = tipo.id_tipo_usuario AND usua.usuario = ? AND usua.contrasenia = ?;",array($user,$password))->row_array();
            if (!empty($loginResponse)) {
                if ($loginResponse['id_tipo_usuario']==1) {
                    $data["response"] = "success";
                    $data["message"] = "Login Exitoso..!";
                    $data["data"] = array(
                        "tipo"          => mb_strtolower('sadmin'),
                        "id_usuario"    => @$loginResponse['id_usuario'],
                        "id_registro"   => @$loginResponse['id_cliente'],
                        "id_tipo_usuario"   => @$loginResponse['id_tipo_usuario'],
                        "usuario"   => @$loginResponse['usuario'],
                        "nombres"       => @$loginResponse['nombres_cliente'],
                        "apellidos"     => @$loginResponse['apellidos_cliente'],
                        "email"         => @$loginResponse['email_cliente'],
                        "user"          => @$loginResponse['usuario'],
                    );
                }else if ($loginResponse['id_tipo_usuario']==2) {
                    $data["response"] = "success";
                    $data["message"] = "Login Exitoso..!";
                    $data["data"] = array(
                        "tipo"          => mb_strtolower('admin'),
                        "id_usuario"    => @$loginResponse['id_usuario'],
                        "id_registro"   => @$loginResponse['id_cliente'],
                        "id_tipo_usuario"   => @$loginResponse['id_tipo_usuario'],
                        "usuario"   => @$loginResponse['usuario'],
                        "nombres"       => @$loginResponse['nombres_cliente'],
                        "apellidos"     => @$loginResponse['apellidos_cliente'],
                        "email"         => @$loginResponse['email_cliente'],
                        "user"          => @$loginResponse['usuario'],
                    );
                } else if ($loginResponse['id_tipo_usuario']==3) {
                    $data["response"] = "success";
                    $data["message"] = "Login Exitoso..!";
                    $data["data"] = array(
                        "tipo"          => mb_strtolower('user'),
                        "id_usuario"    => @$loginResponse['id_usuario'],
                        "id_registro"   => @$loginResponse['id_cliente'],
                        "id_tipo_usuario"   => @$loginResponse['id_tipo_usuario'],
                        "usuario"   => @$loginResponse['usuario'],
                        "nombres"       => @$loginResponse['nombres_cliente'],
                        "apellidos"     => @$loginResponse['apellidos_cliente'],
                        "email"         => @$loginResponse['email_cliente'],
                        "user"          => @$loginResponse['usuario'],
                    );
                }
                
            }else{
                $data["response"] = "error";
                $data["message"] = "La cuenta no existe..!";
                $data["data"] = array(
                    "tipo"          => null,
                );
            }
        
        return $data;
            // return $loginResponse;
    }
}
