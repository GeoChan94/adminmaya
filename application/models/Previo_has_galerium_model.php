<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Previo_has_galerium_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get previo_has_galerium by id_previo_has_galeriacol
     */
    function get_previo_has_galerium($id_previo_has_galeriacol)
    {
        return $this->db->get_where('previo_has_galeria',array('id_previo_has_galeriacol'=>$id_previo_has_galeriacol))->row_array();
    }
        
    /*
     * Get all previo_has_galeria
     */
    function get_all_previo_has_galeria()
    {
        $this->db->order_by('id_previo_has_galeriacol', 'desc');
        return $this->db->get('previo_has_galeria')->result_array();
    }
        
    /*
     * function to add new previo_has_galerium
     */
    function add_previo_has_galerium($params)
    {
        $this->db->insert('previo_has_galeria',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update previo_has_galerium
     */
    function update_previo_has_galerium($id_previo_has_galeriacol,$params)
    {
        $this->db->where('id_previo_has_galeriacol',$id_previo_has_galeriacol);
        return $this->db->update('previo_has_galeria',$params);
    }
    
    /*
     * function to delete previo_has_galerium
     */
    function delete_previo_has_galerium($id_previo_has_galeriacol)
    {
        return $this->db->delete('previo_has_galeria',array('id_previo_has_galeriacol'=>$id_previo_has_galeriacol));
    }
}
