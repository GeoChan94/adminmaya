<?php

class Galerium_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get galerium by id_galeria
     */
    function get_galerium($id_galeria)
    {
        return $this->db->get_where('galeria',array('id_galeria'=>$id_galeria))->row_array();
    }
        
    /*
     * Get all galeria
     */
    function get_all_galeria()
    {
        $this->db->order_by('id_galeria', 'desc');
        return $this->db->get('galeria')->result_array();
    }
        
    /*
     * function to add new galerium
     */
    function add_galerium($params)
    {
        $this->db->insert('galeria',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update galerium
     */
    function update_galerium($id_galeria,$params)
    {
        $this->db->where('id_galeria',$id_galeria);
        return $this->db->update('galeria',$params);
    }
    
    /*
     * function to delete galerium
     */
    function delete_galerium($id_galeria)
    {
        return $this->db->delete('galeria',array('id_galeria'=>$id_galeria));
    }

    function GaleriaByPredio( $id_predio = null ){
        return $this->db->query("SELECT * FROM previo_has_galeria AS prhasga JOIN galeria AS gal ON prhasga.galeria_id_galeria = gal.id_galeria AND prhasga.previo_id_previo = ?;", array($id_predio))->result_array();
    }

    function get_galeriumExist($id_predio = null, $uri_galeria = null ){
        return $this->db->query("SELECT * FROM previo_has_galeria AS prhasga JOIN galeria AS gal ON prhasga.galeria_id_galeria = gal.id_galeria AND prhasga.previo_id_previo = ? AND gal.uri_galeria = ?;", array($id_predio, $uri_galeria) )->row_array();       
    }
}
