<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'page';

// $route['^propiedades/busqueda'] = 'propiedades/page_alquiler'; //filtro
$route['^alquiler/(:any)'] = 'Propiedades/previo/$1';
$route['^anticresis/(:any)'] = 'Propiedades/previo/$1';
$route['^venta/(:any)'] = 'Propiedades/previo/$1';
$route['^proyecto/(:any)'] = 'Propiedades/previo/$1';

$route['^buscar/(:any)/(:any)'] = 'Propiedades/buscar/$1/$2';

$route['^contacto'] = 'contacto';
// -------------------------------------------


$route['admin/caracteristica/(:num)'] = 'admin/caracteristica/index/$1';
$route['admin/propietario/(:num)'] = 'admin/propietario/index/$1';
$route['admin/mensaje/(:num)'] = 'mensaje/index/$1';
$route['admin/previo_has_servicio/(:num)'] = 'admin/previo_has_servicio/index/$1';
$route['^login'] 							= 'admin/Login';

$route['^admin'] = 'admin/dashboard/index';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
