<?php
$BASE_URL ="http://localhost/gallery/";
$data = null;
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    //$file = $_FILES['archivos']['name'];
    //$nameFile = $_FILES['archivos']['name'];
    $extension = pathinfo($_FILES['archivos']['name']); 
    $file = "img-".date('Ymd-His').".".$extension['extension'];

    if(!is_dir("imagenes/")){ 
      mkdir("imagenes/", 0777);
      //mkdir("imagenes/categoria/", 0777);
    }
    if ($file && move_uploaded_file($_FILES['archivos']['tmp_name'],"imagenes/".$file)){
      //sleep(2);
      $data = array(
        'rpta'    => 'success',
        'message' => 'Successful image',
        'imagen'  => $file,
        'uri_path'=> 'assets/galeria/imagenes/'.$file,
        'url'     => $BASE_URL.'assets/galeria/imagenes/'.$file,
      );
    }else{
      $data = array(
        'rpta' => 'error',
        'message' => 'The image cann\'t be uploaded',
        'url'  => 'https://dummyimage.com/180x120/ccc/000',
      );
    }
}else{
    //throw new Exception("Error Processing Request", 1);  
    $data = array(
      'rpta' => 'error',
      'message' => 'Error connecting to server',
      'url'  => 'https://dummyimage.com/180x120/ccc/000',
    ); 
}

echo json_encode($data);

?>